package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day13"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("FirstFold:   %d\n", day13.FirstFold(input))
	fmt.Printf("AllTheFolds:\n%s", day13.AllTheFolds(input))
}
