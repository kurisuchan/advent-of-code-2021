package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day14"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("PairInsertion10: %d\n", day14.PairInsertion(input, 10))
	fmt.Printf("PairInsertion40: %d\n", day14.PairInsertion(input, 40))
}
