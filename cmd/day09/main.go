package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day09"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("LowPoints: %d\n", day09.LowPoints(input))
	fmt.Printf("Basins:    %d\n", day09.Basins(input))
}
