package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day03"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("PowerConsumption:  %d\n", day03.PowerConsumption(input))
	fmt.Printf("LifeSupportRating: %d\n", day03.LifeSupportRating(input))
}
