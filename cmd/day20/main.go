package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day20"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Enhancement2:  %.0f\n", day20.Enhancement2(input))
	fmt.Printf("Enhancement50: %.0f\n", day20.Enhancement50(input))
}
