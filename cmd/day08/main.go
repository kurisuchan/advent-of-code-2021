package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day08"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("EasyDigits:     %d\n", day08.EasyDigits(input))
	fmt.Printf("DisplayOutputs: %d\n", day08.DisplayOutputs(input))
}
