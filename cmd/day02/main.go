package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day02"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Dive:      %d\n", day02.Dive(input))
	fmt.Printf("AimedDive: %d\n", day02.AimedDive(input))
}
