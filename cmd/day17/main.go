package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day17"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("HighestY:       %d\n", day17.HighestY(input))
	fmt.Printf("TargetPractice: %d\n", day17.TargetPractice(input))
}
