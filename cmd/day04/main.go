package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day04"
)

func main() {
	input := utils2020.ReadStringSpaceGroupsFromFile("input.txt")

	fmt.Printf("BingoSubsystem: %d\n", day04.BingoSubsystem(input))
	fmt.Printf("SquidGame:      %d\n", day04.SquidGame(input))
}
