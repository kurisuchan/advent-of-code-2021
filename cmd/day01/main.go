package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day01"
)

func main() {
	input := utils2020.ReadIntLinesFromFile("input.txt")

	fmt.Printf("DepthIncrease:         %d\n", day01.DepthIncrease(input))
	fmt.Printf("SlidingWindowIncrease: %d\n", day01.SlidingWindowIncrease(input))
}
