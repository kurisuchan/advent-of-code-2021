package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day16"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("PacketChecksum: %d\n", day16.PacketChecksum(input))
	fmt.Printf("PacketDecoder:  %d\n", day16.PacketDecoder(input))
}
