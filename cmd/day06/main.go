package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day06"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Lanternfish80:  %d\n", day06.Lanternfish80(input))
	fmt.Printf("Lanternfish256: %d\n", day06.Lanternfish256(input))
}
