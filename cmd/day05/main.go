package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day05"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("SomeLines: %d\n", day05.SomeLines(input))
	fmt.Printf("AllLines:  %d\n", day05.AllLines(input))
}
