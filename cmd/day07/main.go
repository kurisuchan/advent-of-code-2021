package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day07"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("CrabAlignment:        %d\n", day07.CrabAlignment(input))
	fmt.Printf("ExpensiveCrabShuffle: %d\n", day07.ExpensiveCrabShuffle(input))
}
