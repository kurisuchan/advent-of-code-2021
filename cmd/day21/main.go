package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day21"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("DeterministicZocchihedron: %d\n", day21.DeterministicZocchihedron(input))
	fmt.Printf("QuantumTetrahedon: %d\n", day21.QuantumTetrahedon(input))
}
