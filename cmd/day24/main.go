package main

import (
	"fmt"

	"advent-of-code-2021/pkg/day24"
)

func main() {
	fmt.Printf("LargestModelNumber:  %d\n", day24.LargestModelNumber())
	fmt.Printf("SmallestModelNumber: %d\n", day24.SmallestModelNumber())
}
