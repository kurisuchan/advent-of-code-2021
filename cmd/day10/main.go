package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day10"
)

func main() {
	input := utils2020.NoEmptyStrings(utils2020.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("CorruptNavigation:    %d\n", day10.CorruptNavigation(input))
	fmt.Printf("IncompleteNavigation: %d\n", day10.IncompleteNavigation(input))
}
