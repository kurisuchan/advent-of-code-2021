package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day19"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	part2, part1 := day19.Search(day19.Parse(input))

	fmt.Printf("Beacons:         %d\n", day19.Beacons(part1))
	fmt.Printf("ScannerDistance: %d\n", day19.ScannerDistance(part2))
}
