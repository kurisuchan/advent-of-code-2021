package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day25"
)

func main() {
	input := utils2020.NoEmptyStrings(utils2020.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("SeaCucumberShuffle: %d\n", day25.SeaCucumberShuffle(input))
}
