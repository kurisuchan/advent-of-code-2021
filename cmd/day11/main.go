package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day11"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Flashes:      %d\n", day11.Flashes(input))
	fmt.Printf("Synchronized: %d\n", day11.Synchronized(input))
}
