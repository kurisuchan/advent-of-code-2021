package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day12"
)

func main() {
	input := utils2020.NoEmptyStrings(utils2020.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("PassingPassage:        %d\n", day12.PassingPassage(input))
	fmt.Printf("PassingPluralPassages: %d\n", day12.PassingPluralPassages(input))
}
