package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day23"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("OrganizeSituation: %d\n", day23.OrganizeSituation(input))
	fmt.Printf("SituationUnfolds:  %d\n", day23.SituationUnfolds(input))
}
