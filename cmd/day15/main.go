package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day15"
)

func main() {
	input := utils2020.NoEmptyStrings(utils2020.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("ExitRisk:         %d\n", day15.ExitRisk(input))
	fmt.Printf("ExpandedExitRisk: %d\n", day15.ExpandedExitRisk(input))
}
