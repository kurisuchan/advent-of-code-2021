package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/day18"
)

func main() {
	input := utils2020.NoEmptyStrings(utils2020.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("FishAddition:     %d\n", day18.FishAddition(input))
	fmt.Printf("MaximumFishiness: %d\n", day18.MaximumFishiness(input))
}
