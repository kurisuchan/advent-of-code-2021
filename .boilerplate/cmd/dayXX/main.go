package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/dayXX"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("X: %d\n", dayXX.X(input))
	fmt.Printf("Y: %d\n", dayXX.Y(input))
}
