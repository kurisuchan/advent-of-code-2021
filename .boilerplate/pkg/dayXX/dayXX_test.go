package dayXX_test

import (
	"testing"

	"advent-of-code-2021/pkg/dayXX"
)

func TestX(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := dayXX.X(test.in)
		if actual != test.out {
			t.Errorf("X(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := dayXX.Y(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
