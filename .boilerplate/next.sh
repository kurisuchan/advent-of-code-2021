#!/usr/bin/env bash

DAY=$(date +"%d" --date="tomorrow")
BPDIR=$(dirname "$0")

cp -r ${BPDIR}/cmd/ ${BPDIR}/pkg/ ${BPDIR}/../
sed -i "s/dayXX/day${DAY}/g" ${BPDIR}/../cmd/dayXX/main.go
sed -i "s/dayXX/day${DAY}/g" ${BPDIR}/../pkg/dayXX/*
mv ${BPDIR}/../pkg/dayXX/dayXX.go ${BPDIR}/../pkg/dayXX/day${DAY}.go
mv ${BPDIR}/../pkg/dayXX/dayXX_test.go ${BPDIR}/../pkg/dayXX/day${DAY}_test.go
mv ${BPDIR}/../pkg/dayXX ${BPDIR}/../pkg/day${DAY}
mv ${BPDIR}/../cmd/dayXX ${BPDIR}/../cmd/day${DAY}

git checkout -b day-${DAY}
