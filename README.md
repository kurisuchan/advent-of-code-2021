This repository is a collection of my solutions for the [Advent of Code 2021](http://adventofcode.com/2021) calendar.

| Puzzle                                       | Silver               | Gold                 |
|----------------------------------------------|----------------------|----------------------|
| [Day 1: Sonar Sweep](pkg/day01)              | `00:12:57`  (`5671`) | `00:15:41`  (`4070`) |
| [Day 2: Dive!](pkg/day02)                    | `00:05:49`  (`3872`) | `00:07:56`  (`2814`) |
| [Day 3: Binary Diagnostic](pkg/day03)        | `00:13:39`  (`4706`) | `00:35:58`  (`3382`) |
| [Day 4: Giant Squid](pkg/day04)              | `00:27:31`  (`2250`) | `00:36:12`  (`2140`) |
| [Day 5: Hydrothermal Venture](pkg/day05)     | `00:26:01`  (`3477`) | `00:45:34`  (`3722`) |
| [Day 6: Lanternfish](pkg/day06)              | `00:17:46`  (`6054`) | `00:18:10`  (`1710`) |
| [Day 7: The Treachery of Whales](pkg/day07)  | `00:07:40`  (`3170`) | `00:11:02`  (`2168`) |
| [Day 8: Seven Segment Search](pkg/day08)     | `00:14:48`  (`3908`) | `01:15:04`  (`3137`) |
| [Day 9: Smoke Basin](pkg/day09)              | `00:12:04`  (`2350`) | `00:25:29`  (`1328`) |
| [Day 10: Syntax Scoring](pkg/day10)          | `00:10:57`  (`1813`) | `00:26:55`  (`3029`) |
| [Day 11: Dumbo Octopus](pkg/day11)           | `00:36:35`  (`3319`) | `00:38:43`  (`3024`) |
| [Day 12: Passage Pathing](pkg/day12)         | `00:33:48`  (`3074`) | `01:14:47`  (`4166`) |
| [Day 13: Transparent Origami](pkg/day13)     | `00:22:09`  (`2115`) | `00:33:41`  (`2483`) |
| [Day 14: Extended Polymerization](pkg/day14) | `00:25:32`  (`4463`) | `00:49:25`  (`2169`) |
| [Day 15: Chiton](pkg/day15)                  | `01:15:57`  (`4604`) | `01:56:10`  (`3818`) |
| [Day 16: Packet Decoder](pkg/day16)          | `01:57:06`  (`3968`) | `02:06:29`  (`3287`) |
| [Day 17: Trick Shot](pkg/day17)              | `00:32:56`  (`2155`) | `00:34:49`  (`1384`) |
| [Day 18: Snailfish](pkg/day18)               | `03:00:44`  (`2426`) | `03:03:52`  (`2300`) |
| [Day 19: Beacon Scanner](pkg/day19)          | `02:21:05`  (`1009`) | `02:25:09`  (` 887`) |
| [Day 20: Trench Map](pkg/day20)              | `00:32:02`  (` 759`) | `00:33:07`  (` 601`) |
| [Day 21: Dirac Dice](pkg/day21)              | `00:23:57`  (`2511`) | `00:44:09`  (` 822`) |
| [Day 22: Reactor Reboot](pkg/day22)          | `00:20:17`  (`1709`) | `02:23:10`  (`1437`) |
| [Day 23: Amphipod](pkg/day23)[^1]            | `13:31:19`  (`7513`) | `13:41:44`  (`4601`) |
| [Day 24: Arithmetic Logic Unit](pkg/day24)   | `04:48:43`  (`1829`) | `05:00:56`  (`1805`) |
| [Day 25: Sea Cucumber](pkg/day25)            | `00:22:25`  (` 991`) | `00:22:53`  (` 824`) |


[^1]: includes 8 hours not spent trying to solve
