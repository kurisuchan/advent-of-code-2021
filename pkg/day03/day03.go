package day03

import "strconv"

func PowerConsumption(input []string) int {
	var gammaRate, epsilonRate int
	bits := len(input[0])

	for pos := 0; pos < bits; pos++ {
		gammaRate <<= 1
		epsilonRate <<= 1

		count := CountChars(input, pos)
		if count['1'] > count['0'] {
			gammaRate += 1
		} else {
			epsilonRate += 1
		}
	}

	return gammaRate * epsilonRate
}

func LifeSupportRating(input []string) int {
	return BitCriteria(input, OxygenGeneratorRating) * BitCriteria(input, CO2ScrubberRating)
}

func CountChars(input []string, pos int) map[uint8]int {
	count := make(map[uint8]int)

	for _, line := range input {
		if len(line) <= pos {
			continue
		}
		count[line[pos]]++
	}

	return count
}

func BitCriteria(input []string, rating Rating) int {
	bits := len(input[0])

	for pos := 0; pos < bits; pos++ {
		if len(input) == 1 {
			break
		}

		count := CountChars(input, pos)
		keep := rating(count['1'], count['0'])

		var newInput []string
		for _, line := range input {
			if len(line) <= pos {
				continue
			}

			if line[pos] == keep {
				newInput = append(newInput, line)
			}
		}
		input = newInput
	}

	n, _ := strconv.ParseInt(input[0], 2, 64)
	return int(n)
}

type Rating func(int, int) uint8

func OxygenGeneratorRating(count1, count0 int) uint8 {
	if count0 > count1 {
		return '0'
	}
	return '1'
}

func CO2ScrubberRating(count1, count0 int) uint8 {
	if count0 > count1 {
		return '1'
	}
	return '0'
}
