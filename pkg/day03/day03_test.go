package day03_test

import (
	"testing"

	"advent-of-code-2021/pkg/day03"
)

func TestPowerConsumption(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"00100",
			"11110",
			"10110",
			"10111",
			"10101",
			"01111",
			"00111",
			"11100",
			"10000",
			"11001",
			"00010",
			"01010",
			"",
		}, 198},
	}

	for _, test := range tests {
		actual := day03.PowerConsumption(test.in)
		if actual != test.out {
			t.Errorf("PowerConsumption(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestLifeSupportRating(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"00100",
			"11110",
			"10110",
			"10111",
			"10101",
			"01111",
			"00111",
			"11100",
			"10000",
			"11001",
			"00010",
			"01010",
			"",
		}, 230},
	}

	for _, test := range tests {
		actual := day03.LifeSupportRating(test.in)
		if actual != test.out {
			t.Errorf("LifeSupportRating(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
