package day23_test

import (
	"testing"

	"advent-of-code-2021/pkg/day23"
)

func TestOrganizeSituation(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"#############\n#...........#\n###A#B#C#D###\n  #A#B#C#D#\n  #########", 0},
		{"#############\n#.........A.#\n###.#B#C#D###\n  #A#B#C#D#\n  #########", 8},
		{"#############\n#.....D.D.A.#\n###.#B#C#.###\n  #A#B#C#.#\n  #########", 7008},
		{"#############\n#.....D.....#\n###.#B#C#D###\n  #A#B#C#A#\n  #########", 9011},
		{"#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########", 12521},
	}

	for _, test := range tests {
		actual := day23.OrganizeSituation(test.in)
		if actual != test.out {
			t.Errorf("OrganizeSituation(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSituationUnfolds(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########", 44169},
	}

	for _, test := range tests {
		actual := day23.SituationUnfolds(test.in)
		if actual != test.out {
			t.Errorf("SituationUnfolds(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestBurrow_Unfold(t *testing.T) {
	tests := []struct {
		in  string
		out day23.State
	}{
		{
			"#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########",
			day23.State{
				H1: '.',
				H2: '.',
				H3: '.',
				H4: '.',
				H5: '.',
				H6: '.',
				H7: '.',
				S1: 'B',
				S2: 'D',
				S3: 'C',
				S4: 'C',
				S5: 'B',
				S6: 'B',
				S7: 'D',
				S8: 'A',
				E1: 'D',
				E2: 'A',
				E3: 'B',
				E4: 'D',
				E5: 'A',
				E6: 'C',
				E7: 'C',
				E8: 'A',
			},
		},
	}

	for _, test := range tests {
		burrow := day23.Parse(test.in)
		burrow.Unfold()
		actual := burrow.State()
		if actual != test.out {
			t.Errorf("Burrow.Unfold(%q) =>\n%+v, want\n%+v", test.in, actual, test.out)
		}
	}
}
