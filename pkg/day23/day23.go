package day23

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/utils"
	"math"
	"strings"
)

var costs = map[rune]int{
	'A': 1,
	'B': 10,
	'C': 100,
	'D': 1000,
}

func OrganizeSituation(input string) int {
	burrow := Parse(input)
	return burrow.Organize()
}

func SituationUnfolds(input string) int {
	burrow := Parse(input)

	burrow.Unfold()

	return burrow.Organize()
}

type Position map[utils2020.Coordinate2]rune

type Burrow struct {
	Hallway  Position
	SideRoom Position
	Tally    int
}

type State struct {
	H1, H2, H3, H4, H5, H6, H7     rune
	S1, S2, S3, S4, S5, S6, S7, S8 rune
	E1, E2, E3, E4, E5, E6, E7, E8 rune
}

type Move struct {
	From utils2020.Coordinate2
	To   utils2020.Coordinate2
	Who  rune
	Cost int
}

func Parse(input string) Burrow {
	burrow := Burrow{
		Hallway:  make(Position),
		SideRoom: make(Position),
	}

	grid := make(map[utils2020.Coordinate2]rune)

	for y, line := range strings.Split(input, "\n") {
		for x, r := range line {
			pos := utils2020.Coordinate2{X: x, Y: y}
			switch r {
			case '#', '.':
				grid[pos] = r
			case 'A', 'B', 'C', 'D':
				grid[pos] = r
			}
		}
	}

	for pos, r := range grid {
		switch r {
		case '.':
			if pos.Y == 1 {
				if grid[pos.Move(utils2020.South)] == '#' {
					burrow.Hallway[pos] = '.'
				}
			} else {
				burrow.SideRoom[pos] = '.'
			}
		case '#':
			continue
		case 'A', 'B', 'C', 'D':
			if pos.Y == 1 {
				burrow.Hallway[pos] = r
			} else {
				burrow.SideRoom[pos] = r
			}
		}
	}

	return burrow
}

func (b Burrow) PossibleMoves() []Move {
	var moves []Move

	for from, fr := range b.SideRoom {
		if fr == '.' || b.BlockedAbove(from) {
			continue
		}

		// from side room to hallway
		for to, tr := range b.Hallway {
			if tr != '.' {
				continue
			}
			if !b.BlockedHallway(from, to) {
				moves = append(moves, Move{
					From: from,
					To:   to,
					Who:  fr,
					Cost: costs[fr] * from.Distance(to),
				})
			}
		}

		// from side room to side room
		for to, tr := range b.SideRoom {
			if tr != '.' || b.BlockedAbove(to) || b.BlockedHallway(from, to) || !b.ValidRoom(fr, to) || b.BlockingBelow(to) {
				continue
			}
			moves = append(moves, Move{
				From: from,
				To:   to,
				Who:  fr,
				Cost: costs[fr] * (from.Y - 1 + to.Y - 1 + utils2020.AbsInt(from.X-to.X) + 1),
			})
		}
	}

	for from, fr := range b.Hallway {
		if fr == '.' {
			continue
		}

		// from hallway to side room
		for to, tr := range b.SideRoom {
			if tr != '.' || b.BlockedHallway(from, to) || !b.ValidRoom(fr, to) || b.BlockingBelow(to) {
				continue
			}
			moves = append(moves, Move{
				From: from,
				To:   to,
				Who:  fr,
				Cost: costs[fr] * from.Distance(to),
			})
		}
	}

	return moves
}

func (b Burrow) BlockedAbove(pos utils2020.Coordinate2) bool {
	for y := 2; y < pos.Y; y++ {
		if r, ok := b.SideRoom[utils2020.Coordinate2{X: pos.X, Y: y}]; ok && r != '.' {
			return true
		}
	}
	return false
}

func (b Burrow) BlockingBelow(pos utils2020.Coordinate2) bool {
	for y := 2; y <= 5; y++ {
		if r, ok := b.SideRoom[utils2020.Coordinate2{X: pos.X, Y: y}]; ok && r != '.' {
			return true
		}
	}
	return false
}

func (b Burrow) BlockedHallway(from, to utils2020.Coordinate2) bool {
	for x := utils.MinInt(from.X, to.X); x <= utils.MaxInt(from.X, to.X); x++ {
		if x == from.X {
			continue
		}
		if r, ok := b.Hallway[utils2020.Coordinate2{X: x, Y: 1}]; ok && r != '.' {
			return true
		}
	}
	return false
}

func (b Burrow) ValidRoom(r rune, pos utils2020.Coordinate2) bool {
	if !IsHome(r, pos) {
		return false
	}
	pos.Y++
	if t, ok := b.SideRoom[pos]; ok && r != t {
		return false
	}
	return true
}

func IsHome(pod rune, pos utils2020.Coordinate2) bool {
	switch pod {
	case 'A':
		if pos.X == 3 {
			return true
		}
	case 'B':
		if pos.X == 5 {
			return true
		}
	case 'C':
		if pos.X == 7 {
			return true
		}
	case 'D':
		if pos.X == 9 {
			return true
		}
	}
	return false
}

func (b Burrow) Solved() bool {
	return len(b.SideRoom) == 0
}

func (b Burrow) State() State {
	return State{
		H1: b.Hallway[utils2020.Coordinate2{X: 1, Y: 1}],
		H2: b.Hallway[utils2020.Coordinate2{X: 2, Y: 1}],
		H3: b.Hallway[utils2020.Coordinate2{X: 4, Y: 1}],
		H4: b.Hallway[utils2020.Coordinate2{X: 6, Y: 1}],
		H5: b.Hallway[utils2020.Coordinate2{X: 8, Y: 1}],
		H6: b.Hallway[utils2020.Coordinate2{X: 10, Y: 1}],
		H7: b.Hallway[utils2020.Coordinate2{X: 11, Y: 1}],
		S1: b.SideRoom[utils2020.Coordinate2{X: 3, Y: 2}],
		S2: b.SideRoom[utils2020.Coordinate2{X: 3, Y: 3}],
		S3: b.SideRoom[utils2020.Coordinate2{X: 5, Y: 2}],
		S4: b.SideRoom[utils2020.Coordinate2{X: 5, Y: 3}],
		S5: b.SideRoom[utils2020.Coordinate2{X: 7, Y: 2}],
		S6: b.SideRoom[utils2020.Coordinate2{X: 7, Y: 3}],
		S7: b.SideRoom[utils2020.Coordinate2{X: 9, Y: 2}],
		S8: b.SideRoom[utils2020.Coordinate2{X: 9, Y: 3}],
		E1: b.SideRoom[utils2020.Coordinate2{X: 3, Y: 4}],
		E2: b.SideRoom[utils2020.Coordinate2{X: 3, Y: 5}],
		E3: b.SideRoom[utils2020.Coordinate2{X: 5, Y: 4}],
		E4: b.SideRoom[utils2020.Coordinate2{X: 5, Y: 5}],
		E5: b.SideRoom[utils2020.Coordinate2{X: 7, Y: 4}],
		E6: b.SideRoom[utils2020.Coordinate2{X: 7, Y: 5}],
		E7: b.SideRoom[utils2020.Coordinate2{X: 9, Y: 4}],
		E8: b.SideRoom[utils2020.Coordinate2{X: 9, Y: 5}],
	}
}

func (b Burrow) Move(move Move) Burrow {
	newBurrow := Burrow{
		Hallway:  make(Position),
		SideRoom: make(Position),
	}

	for pos, r := range b.SideRoom {
		if pos == move.From {
			newBurrow.SideRoom[pos] = '.'
			newBurrow.Tally += move.Cost
		} else if pos == move.To {
			// moved into final position, no longer necessary to keep track of the room
		} else {
			newBurrow.SideRoom[pos] = r
		}
	}

	for pos, r := range b.Hallway {
		if pos == move.From {
			newBurrow.Hallway[pos] = '.'
			newBurrow.Tally += move.Cost
		} else if pos == move.To {
			newBurrow.Hallway[pos] = move.Who
		} else {
			newBurrow.Hallway[pos] = r
		}
	}

	return newBurrow
}

func (b Burrow) Organize() int {
	// remove amphipods already in final position
	for y := 5; y >= 2; y-- {
		for x := 3; x <= 9; x++ {
			pos := utils2020.Coordinate2{X: x, Y: y}
			if r, ok := b.SideRoom[pos]; ok && r != '.' && b.ValidRoom(r, pos) {
				delete(b.SideRoom, pos)
			}
		}
	}

	return b.Solve(make(map[State]int))
}

func (b Burrow) Solve(cache map[State]int) int {
	if b.Solved() {
		return b.Tally
	}

	state := b.State()

	if v, ok := cache[state]; ok && v < b.Tally {
		// we arrived at this state cheaper
		return math.MaxInt
	}

	cache[state] = b.Tally

	cheapest := math.MaxInt
	moves := b.PossibleMoves()

	for _, move := range moves {
		n := b.Move(move)
		cost := n.Solve(cache)

		if cost < cheapest {
			cheapest = cost
		}
	}

	if cheapest != math.MaxInt {
		return cheapest + b.Tally
	}

	return cheapest
}

func (b *Burrow) Unfold() {
	newRooms := Position{
		utils2020.Coordinate2{X: 3, Y: 3}: 'D', // S2
		utils2020.Coordinate2{X: 3, Y: 4}: 'D', // S4
		utils2020.Coordinate2{X: 5, Y: 3}: 'C', // S6
		utils2020.Coordinate2{X: 5, Y: 4}: 'B', // S8
		utils2020.Coordinate2{X: 7, Y: 3}: 'B', // E1
		utils2020.Coordinate2{X: 7, Y: 4}: 'A', // E3
		utils2020.Coordinate2{X: 9, Y: 3}: 'A', // E5
		utils2020.Coordinate2{X: 9, Y: 4}: 'C', // E9
	}
	for pos, r := range b.SideRoom {
		switch pos.Y {
		case 2:
			newRooms[pos] = r
		case 3:
			pos.Y += 2
			newRooms[pos] = r
		}
	}
	b.SideRoom = newRooms
}
