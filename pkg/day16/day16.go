package day16

import (
	"math"
	"strconv"
	"strings"
)

func PacketChecksum(input string) int {
	packet := Parse(input)
	return packet.SumVersions()
}

func PacketDecoder(input string) int {
	packet := Parse(input)
	return packet.Value()
}

func Parse(input string) Packet {
	// :-D
	input = strings.ReplaceAll(input, "0", "0000")
	input = strings.ReplaceAll(input, "1", "0001")
	input = strings.ReplaceAll(input, "2", "0010")
	input = strings.ReplaceAll(input, "3", "0011")
	input = strings.ReplaceAll(input, "4", "0100")
	input = strings.ReplaceAll(input, "5", "0101")
	input = strings.ReplaceAll(input, "6", "0110")
	input = strings.ReplaceAll(input, "7", "0111")
	input = strings.ReplaceAll(input, "8", "1000")
	input = strings.ReplaceAll(input, "9", "1001")
	input = strings.ReplaceAll(input, "A", "1010")
	input = strings.ReplaceAll(input, "B", "1011")
	input = strings.ReplaceAll(input, "C", "1100")
	input = strings.ReplaceAll(input, "D", "1101")
	input = strings.ReplaceAll(input, "E", "1110")
	input = strings.ReplaceAll(input, "F", "1111")
	packet, _ := DecodePacket(input)
	return packet
}

type Packet struct {
	Version      int
	TypeId       int
	LiteralValue int
	Packets      []Packet
}

func DecodePacket(input string) (Packet, string) {
	packet := Packet{
		Version: MustInt(input[0:3]),
		TypeId:  MustInt(input[3:6]),
	}

	var remainder string

	switch packet.TypeId {
	case 4:
		pos := 6
		for input[pos] != '0' {
			pos += 5
		}
		pos += 5

		packet.LiteralValue = DecodeLiteral(input[6:pos])

		remainder = input[pos:]

	default:
		var subPacket Packet

		switch input[6] {
		case '0':
			l := MustInt(input[7:22])
			remainder = input[22 : 22+l]

			for strings.TrimRight(remainder, "0") != "" {
				subPacket, remainder = DecodePacket(remainder)
				packet.Packets = append(packet.Packets, subPacket)
			}

			remainder = input[22+l:]

		case '1':
			l := MustInt(input[7:18])
			remainder = input[18:]

			for i := 0; i < l; i++ {
				subPacket, remainder = DecodePacket(remainder)
				packet.Packets = append(packet.Packets, subPacket)
			}
		}
	}

	return packet, remainder
}

func DecodeLiteral(input string) int {
	var s string
	var pos int
	for pos+5 <= len(input) {
		s += input[pos+1 : pos+5]
		pos += 5
	}
	return MustInt(s)
}

func MustInt(input string) int {
	o, _ := strconv.ParseInt(input, 2, 64)
	return int(o)
}

func (p Packet) SumVersions() int {
	sum := p.Version
	for _, s := range p.Packets {
		sum += s.SumVersions()
	}
	return sum
}

func (p Packet) Value() int {
	var out int
	switch p.TypeId {
	case 0:
		for i := range p.Packets {
			out += p.Packets[i].Value()
		}

	case 1:
		out = 1
		for i := range p.Packets {
			out *= p.Packets[i].Value()
		}

	case 2:
		out = math.MaxInt
		for i := range p.Packets {
			val := p.Packets[i].Value()
			if val < out {
				out = val
			}
		}

	case 3:
		for i := range p.Packets {
			val := p.Packets[i].Value()
			if val > out {
				out = val
			}
		}
	case 4:
		out = p.LiteralValue

	case 5:
		if p.Packets[0].Value() > p.Packets[1].Value() {
			out = 1
		}

	case 6:
		if p.Packets[0].Value() < p.Packets[1].Value() {
			out = 1
		}

	case 7:
		if p.Packets[0].Value() == p.Packets[1].Value() {
			out = 1
		}
	}

	return out
}
