package day16_test

import (
	"testing"

	"advent-of-code-2021/pkg/day16"
)

func TestPacketChecksum(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"D2FE28", 6},
		{"38006F45291200", 9},
		{"EE00D40C823060", 14},
		{"8A004A801A8002F478", 16},
		{"620080001611562C8802118E34", 12},
		{"C0015000016115A2E0802F182340", 23},
		{"A0016C880162017C3686B18A3D4780", 31},
	}

	for _, test := range tests {
		actual := day16.PacketChecksum(test.in)
		if actual != test.out {
			t.Errorf("PacketChecksum(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestPacketDecoder(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"D2FE28", 2021},
		{"F600BC7D85", 1},
		{"C200B40A82", 3},
		{"04005AC33890", 54},
		{"880086C3E88112", 7},
		{"CE00C43D881120", 9},
		{"D8005AC2A8F0", 1},
		{"F600BC2D8F", 0},
		{"9C005AC2F8F0", 0},
		{"9C0141080250320F1802104A08", 1},
	}

	for _, test := range tests {
		actual := day16.PacketDecoder(test.in)
		if actual != test.out {
			t.Errorf("PacketDecoder(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
