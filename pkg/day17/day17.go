package day17

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/utils"
	"regexp"
)

func HighestY(input string) int {
	tMin, tMax := Parse(input)

	var finalY int

	for x := 0; x <= tMax.X; x++ {
		for y := tMin.Y; y <= -tMin.Y; y++ {
			var hit, miss bool
			var maxY int

			p := Probe{
				Vel: utils2020.Coordinate2{X: x, Y: y},
			}

			for !hit && !miss {
				p.Step()

				if p.Pos.Y > maxY {
					maxY = p.Pos.Y
				}

				hit, miss = p.Check(tMin, tMax)
			}

			if hit {
				if maxY > finalY {
					finalY = maxY
				}
			}
		}
	}

	return finalY
}

func TargetPractice(input string) int {
	tMin, tMax := Parse(input)

	var hits int

	for x := 0; x <= tMax.X; x++ {
		for y := tMin.Y; y <= -tMin.Y; y++ {
			var hit, miss bool

			p := Probe{
				Vel: utils2020.Coordinate2{X: x, Y: y},
			}

			for !hit && !miss {
				p.Step()

				hit, miss = p.Check(tMin, tMax)
			}

			if hit {
				hits++
			}
		}
	}

	return hits
}

type Probe struct {
	Pos utils2020.Coordinate2
	Vel utils2020.Coordinate2
}

func (p *Probe) Step() {
	p.Pos = p.Pos.Move(p.Vel)
	// ignoring `increases by 1 if it is less than 0`
	if p.Vel.X > 0 {
		p.Vel.X--
	}
	p.Vel.Y--
}

func (p Probe) Check(tMin, tMax utils2020.Coordinate2) (bool, bool) {
	// ignoring negative x positions and movement
	if p.Pos.Y < tMin.Y || p.Pos.X > tMax.X {
		return false, true
	}

	if tMin.X <= p.Pos.X && p.Pos.X <= tMax.X &&
		tMin.Y <= p.Pos.Y && p.Pos.Y <= tMax.Y {
		return true, false
	}

	return false, false
}

var targetMatch = regexp.MustCompile(`target area: x=(\d+)..(\d+), y=(-\d+)..(-\d+)`)

func Parse(input string) (utils2020.Coordinate2, utils2020.Coordinate2) {
	match := targetMatch.FindAllStringSubmatch(input, 1)
	x1 := utils2020.MustInt(match[0][1])
	x2 := utils2020.MustInt(match[0][2])
	y1 := utils2020.MustInt(match[0][3])
	y2 := utils2020.MustInt(match[0][4])
	return utils2020.Coordinate2{
			X: utils.MinInt(x1, x2),
			Y: utils.MinInt(y1, y2),
		}, utils2020.Coordinate2{
			X: utils.MaxInt(x1, x2),
			Y: utils.MaxInt(y1, y2),
		}
}
