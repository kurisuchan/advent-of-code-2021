package day17_test

import (
	"testing"

	"advent-of-code-2021/pkg/day17"
)

func TestHighestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"target area: x=20..30, y=-10..-5", 45},
	}

	for _, test := range tests {
		actual := day17.HighestY(test.in)
		if actual != test.out {
			t.Errorf("HighestY(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTargetPractice(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"target area: x=20..30, y=-10..-5", 112},
	}

	for _, test := range tests {
		actual := day17.TargetPractice(test.in)
		if actual != test.out {
			t.Errorf("TargetPractice(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
