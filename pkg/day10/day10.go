package day10

import (
	"sort"
)

var closers = map[rune]rune{
	'(': ')',
	'[': ']',
	'{': '}',
	'<': '>',
}

var illegalScore = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}

func CorruptNavigation(input []string) int {
	var score int

	for _, line := range input {
		var expected []rune

		for _, r := range line {
			if closer, ok := closers[r]; ok {
				expected = append([]rune{closer}, expected...)
			} else {
				if expected[0] == r {
					expected = expected[1:]
				} else {
					score += illegalScore[r]
					break
				}
			}
		}
	}

	return score
}

func IncompleteNavigation(input []string) int {
	var scores []int

	for _, line := range input {
		var expected []rune
		var corrupt bool

		for _, r := range line {
			if closer, ok := closers[r]; ok {
				expected = append([]rune{closer}, expected...)
			} else {
				if expected[0] == r {
					expected = expected[1:]
				} else {
					corrupt = true
					break
				}
			}
		}

		if !corrupt {
			scores = append(scores, Score(expected))
		}
	}

	sort.Ints(scores)

	return scores[len(scores)/2]
}

var corruptScore = map[rune]int{
	')': 1,
	']': 2,
	'}': 3,
	'>': 4,
}

func Score(completion []rune) int {
	var score int
	for _, r := range completion {
		score *= 5
		score += corruptScore[r]
	}
	return score
}
