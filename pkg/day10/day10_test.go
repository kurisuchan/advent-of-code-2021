package day10_test

import (
	"testing"

	"advent-of-code-2021/pkg/day10"
)

func TestCorruptNavigation(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"[({(<(())[]>[[{[]{<()<>>",
			"[(()[<>])]({[<{<<[]>>(",
			"{([(<{}[<>[]}>{[]{[(<()>",
			"(((({<>}<{<{<>}{[]{[]{}",
			"[[<[([]))<([[{}[[()]]]",
			"[{[{({}]{}}([{[{{{}}([]",
			"{<[[]]>}<{[{[{[]{()[[[]",
			"[<(<(<(<{}))><([]([]()",
			"<{([([[(<>()){}]>(<<{{",
			"<{([{{}}[<[[[<>{}]]]>[]]",
		}, 26397},
	}

	for _, test := range tests {
		actual := day10.CorruptNavigation(test.in)
		if actual != test.out {
			t.Errorf("CorruptNavigation(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestIncompleteNavigation(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"[({(<(())[]>[[{[]{<()<>>",
			"[(()[<>])]({[<{<<[]>>(",
			"{([(<{}[<>[]}>{[]{[(<()>",
			"(((({<>}<{<{<>}{[]{[]{}",
			"[[<[([]))<([[{}[[()]]]",
			"[{[{({}]{}}([{[{{{}}([]",
			"{<[[]]>}<{[{[{[]{()[[[]",
			"[<(<(<(<{}))><([]([]()",
			"<{([([[(<>()){}]>(<<{{",
			"<{([{{}}[<[[[<>{}]]]>[]]",
		}, 288957},
	}

	for _, test := range tests {
		actual := day10.IncompleteNavigation(test.in)
		if actual != test.out {
			t.Errorf("IncompleteNavigation(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestScore(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"}}]])})]", 288957},
		{")}>]})", 5566},
		{"}}>}>))))", 1480781},
		{"]]}}]}]}>", 995444},
		{"])}>", 294},
	}

	for _, test := range tests {
		actual := day10.Score([]rune(test.in))
		if actual != test.out {
			t.Errorf("Score(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
