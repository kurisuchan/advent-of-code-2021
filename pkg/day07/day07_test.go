package day07_test

import (
	"testing"

	"advent-of-code-2021/pkg/day07"
)

func TestCrabAlignment(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"16,1,2,0,4,2,7,1,2,14", 37},
	}

	for _, test := range tests {
		actual := day07.CrabAlignment(test.in)
		if actual != test.out {
			t.Errorf("CrabAlignment(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestExpensiveCrabShuffle(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"16,1,2,0,4,2,7,1,2,14", 168},
	}

	for _, test := range tests {
		actual := day07.ExpensiveCrabShuffle(test.in)
		if actual != test.out {
			t.Errorf("ExpensiveCrabShuffle(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
