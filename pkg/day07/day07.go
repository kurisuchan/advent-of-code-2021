package day07

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/utils"
	"math"
	"strings"
)

func CrabAlignment(input string) int {
	positions, max := GetCrabs(input)
	minFuel := math.MaxInt

	for target := 0; target <= max; target++ {
		var fuel int

		for _, pos := range positions {
			if pos < target {
				fuel += target - pos
			} else {
				fuel += pos - target
			}
		}

		if fuel < minFuel {
			minFuel = fuel
		}
	}

	return minFuel
}

func ExpensiveCrabShuffle(input string) int {
	positions, max := GetCrabs(input)
	minFuel := math.MaxInt

	for target := 0; target <= max; target++ {
		var fuel int

		for _, pos := range positions {
			if pos < target {
				fuel += utils.TriangularNumber(target - pos)
			} else {
				fuel += utils.TriangularNumber(pos - target)
			}
		}

		if fuel < minFuel {
			minFuel = fuel
		}
	}

	return minFuel
}

func GetCrabs(input string) ([]int, int) {
	var positions []int
	var max int

	for _, num := range strings.Split(input, ",") {
		pos := utils2020.MustInt(strings.TrimSpace(num))
		positions = append(positions, pos)
		if pos > max {
			max = pos
		}
	}

	return positions, max
}
