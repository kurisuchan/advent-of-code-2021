package day12_test

import (
	"testing"

	"advent-of-code-2021/pkg/day12"
)

func TestPassingPassage(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"start-A",
			"start-b",
			"A-c",
			"A-b",
			"b-d",
			"A-end",
			"b-end",
		}, 10},
		{[]string{
			"dc-end",
			"HN-start",
			"start-kj",
			"dc-start",
			"dc-HN",
			"LN-dc",
			"HN-end",
			"kj-sa",
			"kj-HN",
			"kj-dc",
		}, 19},
		{[]string{
			"fs-end",
			"he-DX",
			"fs-he",
			"start-DX",
			"pj-DX",
			"end-zg",
			"zg-sl",
			"zg-pj",
			"pj-he",
			"RW-he",
			"fs-DX",
			"pj-RW",
			"zg-RW",
			"start-pj",
			"he-WI",
			"zg-he",
			"pj-fs",
			"start-RW",
		}, 226},
	}

	for _, test := range tests {
		actual := day12.PassingPassage(test.in)
		if actual != test.out {
			t.Errorf("PassingPassage(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestPassingPluralPassages(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"start-A",
			"start-b",
			"A-c",
			"A-b",
			"b-d",
			"A-end",
			"b-end",
		}, 36},
		{[]string{
			"dc-end",
			"HN-start",
			"start-kj",
			"dc-start",
			"dc-HN",
			"LN-dc",
			"HN-end",
			"kj-sa",
			"kj-HN",
			"kj-dc",
		}, 103},
		{[]string{
			"fs-end",
			"he-DX",
			"fs-he",
			"start-DX",
			"pj-DX",
			"end-zg",
			"zg-sl",
			"zg-pj",
			"pj-he",
			"RW-he",
			"fs-DX",
			"pj-RW",
			"zg-RW",
			"start-pj",
			"he-WI",
			"zg-he",
			"pj-fs",
			"start-RW",
		}, 3509},
	}

	for _, test := range tests {
		actual := day12.PassingPluralPassages(test.in)
		if actual != test.out {
			t.Errorf("PassingPluralPassages(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
