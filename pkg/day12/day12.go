package day12

import (
	"strings"
)

type CaveMap map[string][]string
type DistinctPaths map[string]struct{}

func PassingPassage(input []string) int {
	cm := NewCaveMap(input)

	paths := cm.Paths(false)

	return len(paths)
}

func PassingPluralPassages(input []string) int {
	cm := NewCaveMap(input)

	paths := cm.Paths(true)

	return len(paths)
}

func NewCaveMap(input []string) CaveMap {
	cm := make(CaveMap)

	for _, line := range input {
		parts := strings.Split(line, "-")
		if parts[0] != "end" && parts[1] != "start" {
			cm[parts[0]] = append(cm[parts[0]], parts[1])
		}
		if parts[0] != "start" && parts[1] != "end" {
			cm[parts[1]] = append(cm[parts[1]], parts[0])
		}
	}

	return cm
}

func (cm CaveMap) Paths(twice bool) DistinctPaths {
	paths := make(DistinctPaths)
	visited := make(map[string]int)
	var path []string

	if twice {
		for cave := range cm {
			if !IsBig(cave) && cave != "start" && cave != "end" {
				cm.Traverse("start", "end", cave, visited, path, paths)
			}
		}
	} else {
		cm.Traverse("start", "end", "", visited, path, paths)
	}

	return paths
}

func (cm CaveMap) Traverse(from, to, twice string, visited map[string]int, path []string, paths map[string]struct{}) {
	visited[from]++
	path = append(path, from)

	if from == to {
		paths[strings.Join(path, ",")] = struct{}{}
	}

	for _, target := range cm[from] {
		if visited[target] == 0 || IsBig(target) || (twice == target && visited[target] == 1) {
			cm.Traverse(target, to, twice, visited, path, paths)
		}
	}

	path = path[:len(path)-1]
	visited[from]--
}

func IsBig(s string) bool {
	return s[0] < 'a'
}
