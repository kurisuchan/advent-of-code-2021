package day02_test

import (
	"testing"

	"advent-of-code-2021/pkg/day02"
)

func TestDive(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"forward 5",
			"down 5",
			"forward 8",
			"up 3",
			"down 8",
			"forward 2",
		}, 150},
	}

	for _, test := range tests {
		actual := day02.Dive(test.in)
		if actual != test.out {
			t.Errorf("Dive(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAimedDive(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"forward 5",
			"down 5",
			"forward 8",
			"up 3",
			"down 8",
			"forward 2",
		}, 900},
	}

	for _, test := range tests {
		actual := day02.AimedDive(test.in)
		if actual != test.out {
			t.Errorf("AimedDive(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
