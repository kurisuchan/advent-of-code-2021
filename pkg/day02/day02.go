package day02

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

const (
	forward = "forward"
	down    = "down"
	up      = "up"
)

func ParseInput(input string) (string, int) {
	parts := strings.Split(input, " ")
	return parts[0], utils2020.MustInt(parts[1])
}


func Dive(input []string) int {
	var horizontal, depth int

	for _, line := range input {
		ins, num := ParseInput(line)

		switch ins {
		case forward:
			horizontal += num
		case down:
			depth += num
		case up:
			depth -= num
		}
	}

	return horizontal * depth
}

func AimedDive(input []string) int {
	var horizontal, depth, aim int

	for _, line := range input {
		ins, num := ParseInput(line)

		switch ins {
		case forward:
			horizontal += num
			depth += aim * num
		case down:
			aim += num
		case up:
			aim -= num
		}
	}

	return horizontal * depth
}
