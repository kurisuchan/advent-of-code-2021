package day04

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

func BingoSubsystem(input [][]string) int {
	boards := prep(input)

	for _, num := range strings.Split(input[0][0], ",") {
		number := utils2020.MustInt(num)
		for _, board := range boards {
			if board.Call(number) {
				return board.Score(number)
			}
		}
	}

	return 0
}

func SquidGame(input [][]string) int {
	boards := prep(input)

	for _, num := range strings.Split(input[0][0], ",") {
		number := utils2020.MustInt(num)
		for i := len(boards) - 1; i >= 0; i-- {
			if boards[i].Call(number) {
				if len(boards) == 1 {
					return boards[i].Score(number)
				} else {
					boards = append(boards[:i], boards[i+1:]...)
				}
			}
		}
	}

	return 0
}

func prep(input [][]string) []*Bingo {
	var boards []*Bingo

	for i := 1; i < len(input); i++ {
		if len(input[i]) != BoardSize {
			continue
		}
		boards = append(boards, NewBoard(input[i]))
	}

	return boards
}
