package day04

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

const BoardSize = 5

type Bingo [BoardSize][BoardSize]struct {
	Number int
	Called bool
}

func NewBoard(input []string) *Bingo {
	var b Bingo
	for y, line := range input {
		for x, num := range strings.Fields(line) {
			b[y][x].Number = utils2020.MustInt(num)
		}
	}
	return &b
}

func (b *Bingo) Call(number int) bool {
	for y := 0; y < BoardSize; y++ {
		rowCalled := true
		for x := 0; x < BoardSize; x++ {
			if b[y][x].Number == number {
				b[y][x].Called = true
			}
			if !b[y][x].Called {
				rowCalled = false
			}
		}
		if rowCalled {
			return true
		}
	}

	for x := 0; x < BoardSize; x++ {
		colCalled := true
		for y := 0; y < BoardSize; y++ {
			if !b[y][x].Called {
				colCalled = false
			}
		}
		if colCalled {
			return true
		}
	}

	return false
}

func (b *Bingo) Score(lastCalled int) int {
	var score int

	for y := 0; y < BoardSize; y++ {
		for x := 0; x < BoardSize; x++ {
			if !b[y][x].Called {
				score += b[y][x].Number
			}
		}
	}

	return score * lastCalled
}
