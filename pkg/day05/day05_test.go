package day05_test

import (
	"testing"

	"advent-of-code-2021/pkg/day05"
)

func TestSomeLines(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{
			"0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2",
			5,
		},
	}

	for _, test := range tests {
		actual := day05.SomeLines(test.in)
		if actual != test.out {
			t.Errorf("SomeLines(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAllLines(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{
			"0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2",
			12,
		},
	}

	for _, test := range tests {
		actual := day05.AllLines(test.in)
		if actual != test.out {
			t.Errorf("AllLines(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
