package day05

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"regexp"
)

var pattern = regexp.MustCompile(`(\d+),(\d+) -> (\d+),(\d+)`)

func SomeLines(input string) int {
	return Overlap(input, true)
}

func AllLines(input string) int {
	return Overlap(input, false)
}

func Overlap(input string, limited bool) int {
	grid := make(map[utils2020.Coordinate2]int)

	coords := Parse(input)

	for l := range coords {
		from, to := coords[l][0], coords[l][1]

		if limited && !(from.X == to.X || from.Y == to.Y) {
			continue
		}

		x, y := from.X, from.Y
		dx, dy := Dir(from.X, to.X), Dir(from.Y, to.Y)

		for {
			grid[utils2020.Coordinate2{X: x, Y: y}]++

			if x == to.X && y == to.Y {
				break
			}

			x += dx
			y += dy
		}
	}

	var count int
	for _, hits := range grid {
		if hits > 1 {
			count++
		}
	}

	return count
}

func Parse(input string) [][2]utils2020.Coordinate2 {
	match := pattern.FindAllStringSubmatch(input, -1)

	var coords [][2]utils2020.Coordinate2
	for l := range match {
		coords = append(coords, [2]utils2020.Coordinate2{
			{
				X: utils2020.MustInt(match[l][1]),
				Y: utils2020.MustInt(match[l][2]),
			},
			{
				X: utils2020.MustInt(match[l][3]),
				Y: utils2020.MustInt(match[l][4]),
			},
		})
	}
	return coords
}

func Dir(a, b int) int {
	if a < b {
		return 1
	} else if a > b {
		return -1
	}
	return 0
}
