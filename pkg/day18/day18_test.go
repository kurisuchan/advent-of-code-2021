package day18_test

import (
	"testing"

	"advent-of-code-2021/pkg/day18"
)

func TestFishAddition(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{"[[1,2],[[3,4],5]]"}, 143},
		{[]string{"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"}, 1384},
		{[]string{"[[[[1,1],[2,2]],[3,3]],[4,4]]"}, 445},
		{[]string{"[[[[3,0],[5,3]],[4,4]],[5,5]]"}, 791},
		{[]string{"[[[[5,0],[7,4]],[5,5]],[6,6]]"}, 1137},
		{[]string{"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"}, 3488},
		{[]string{"[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"}, 4140},
		{[]string{
			"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
			"[[[5,[2,8]],4],[5,[[9,9],0]]]",
			"[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
			"[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
			"[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
			"[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
			"[[[[5,4],[7,7]],8],[[8,3],8]]",
			"[[9,3],[[9,9],[6,[4,9]]]]",
			"[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
			"[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
		}, 4140},
	}

	for _, test := range tests {
		actual := day18.FishAddition(test.in)
		if actual != test.out {
			t.Errorf("FishAddition(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMaximumFishiness(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
			"[[[5,[2,8]],4],[5,[[9,9],0]]]",
			"[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
			"[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
			"[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
			"[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
			"[[[[5,4],[7,7]],8],[[8,3],8]]",
			"[[9,3],[[9,9],[6,[4,9]]]]",
			"[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
			"[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
		}, 3993},
	}

	for _, test := range tests {
		actual := day18.MaximumFishiness(test.in)
		if actual != test.out {
			t.Errorf("MaximumFishiness(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestNewSnailfishNumber(t *testing.T) {
	tests := []struct {
		in string
	}{
		{"[1,2]"},
		{"[[1,2],3]"},
		{"[9,[8,7]]"},
		{"[[1,9],[8,5]]"},
		{"[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"},
		{"[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"},
		{"[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]"},
	}

	for _, test := range tests {
		actual := day18.NewSnailfishNumber(test.in)
		if actual.String() != test.in {
			t.Errorf("NewSnailfishNumber(%q) => %q", test.in, actual.String())
		}
	}
}

func TestSnailfishNumber_Explode(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"},
		{"[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"},
		{"[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]"},
		{"[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"},
		{"[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"},
		{"[[[[[1,1],[2,2]],[3,3]],[4,4]],[5,5]]", "[[[[0,[3,2]],[3,3]],[4,4]],[5,5]]"},
		{"[[[[0,[3,2]],[3,3]],[4,4]],[5,5]]", "[[[[3,0],[5,3]],[4,4]],[5,5]]"},
	}

	for _, test := range tests {
		actual, _ := day18.Parse(test.in[1:])
		actual.Explode(0)
		if actual.String() != test.out {
			t.Errorf("Explode(%q) => %q, want %q", test.in, actual.String(), test.out)
		}
	}
}

func TestSnailfishNumber_Reduce(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"},
	}

	for _, test := range tests {
		actual, _ := day18.Parse(test.in[1:])
		actual.Reduce()
		if actual.String() != test.out {
			t.Errorf("Reduce(%q) => %q, want %q", test.in, actual.String(), test.out)
		}
	}
}
