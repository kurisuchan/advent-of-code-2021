package day18

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"fmt"
	"strings"
)

func FishAddition(input []string) int {
	fish := NewSnailfishNumber(input[0])

	for i := 1; i < len(input); i++ {
		fish = Add(fish, NewSnailfishNumber(input[i]))
	}

	return fish.Magnitude()
}

func MaximumFishiness(input []string) int {
	var maxnitude int

	for i := range input {
		for j := range input {
			if i != j {
				mag := Add(
					NewSnailfishNumber(input[i]),
					NewSnailfishNumber(input[j]),
				).Magnitude()
				if mag > maxnitude {
					maxnitude = mag
				}
			}
		}
	}

	return maxnitude
}

type SnailfishNumber struct {
	leftFish    *SnailfishNumber
	rightFish   *SnailfishNumber
	leftNumber  int
	rightNumber int
	parent      *SnailfishNumber
}

func NewSnailfishNumber(input string) *SnailfishNumber {
	fish, _ := Parse(input[1:])
	return fish
}

func Parse(input string) (*SnailfishNumber, string) {
	var fish SnailfishNumber

	switch input[0] {
	case '[':
		fish.leftFish, input = Parse(input[1:])
		fish.leftFish.parent = &fish
	default:
		fish.leftNumber, input = ExtractNumber(input)
	}

	for input[0] == ',' || input[0] == ']' {
		input = input[1:]
	}

	switch input[0] {
	case '[':
		fish.rightFish, input = Parse(input[1:])
		fish.rightFish.parent = &fish
	default:
		fish.rightNumber, input = ExtractNumber(input)
	}

	return &fish, input
}

func Add(a, b *SnailfishNumber) *SnailfishNumber {
	num := &SnailfishNumber{
		leftFish:  a,
		rightFish: b,
	}
	a.parent, b.parent = num, num

	num.Reduce()

	return num
}

func (s SnailfishNumber) String() string {
	var out strings.Builder

	out.WriteRune('[')

	if s.leftFish != nil {
		out.WriteString(s.leftFish.String())
	} else {
		out.WriteString(fmt.Sprintf("%d", s.leftNumber))
	}

	out.WriteRune(',')

	if s.rightFish != nil {
		out.WriteString(s.rightFish.String())
	} else {
		out.WriteString(fmt.Sprintf("%d", s.rightNumber))
	}

	out.WriteRune(']')

	return out.String()
}

func ExtractNumber(input string) (int, string) {
	var i int

	for i = 0; i < len(input); i++ {
		if !('0' <= input[i] && input[i] <= '9') {
			break
		}
	}

	return utils2020.MustInt(input[:i]), input[i:]
}

func (s *SnailfishNumber) Reduce() {
	split := true
	for split {
		for s.Explode(0) {
		}
		split = s.Split()
	}
}

func (s *SnailfishNumber) Explode(depth int) bool {
	if depth == 4 {
		if s.parent.leftFish == s {
			// we are on the left side of the parent pair

			// move up until we find a parent that doesn't have us on the left side
			parent := s.parent
			node := s
			for parent != nil {
				if parent.leftFish != node {
					break
				}
				node = parent
				parent = parent.parent
			}

			// if there is no valid parent, we are the leftmost element and cannot add our left number
			// if there is a valid parent, we are on the right side of it:
			// - add to the left number or
			// - add to the rightmost child
			if parent != nil {
				if parent.leftFish != nil {
					parent.leftFish.AddRight(s.leftNumber)
				} else {
					parent.leftNumber += s.leftNumber
				}
			}

			// move right number
			if s.parent.rightFish != nil {
				s.parent.rightFish.AddLeft(s.rightNumber)
			} else {
				s.parent.rightNumber += s.rightNumber
			}

			// replace pair with regular number 0
			s.parent.leftFish = nil
			s.parent.leftNumber = 0

		} else if s.parent.rightFish == s {
			// we are on the right side of the parent pair

			// move up until we find a parent that doesn't have us on the right side
			parent := s.parent
			node := s
			for parent != nil {
				if parent.rightFish != node {
					break
				}
				node = parent
				parent = parent.parent
			}

			// if there is no valid parent, we are the rightmost element and cannot add our right number
			// if there is a valid parent, we are on the left side of it:
			// - add to the right number or
			// - add to the leftmost child
			if parent != nil {
				if parent.rightFish != nil {
					parent.rightFish.AddLeft(s.rightNumber)
				} else {
					parent.rightNumber += s.rightNumber
				}
			}

			// move left number
			// sibling to the left has to be a regular number since a pair would have exploded
			s.parent.leftNumber += s.leftNumber

			// replace pair with regular number 0
			s.parent.rightFish = nil
			s.parent.rightNumber = 0
		}
		return true
	}

	if s.leftFish != nil {
		if s.leftFish.Explode(depth + 1) {
			return true
		}
	}

	if s.rightFish != nil {
		return s.rightFish.Explode(depth + 1)
	}

	return false
}

func (s *SnailfishNumber) Split() bool {
	if s.leftFish != nil {
		if s.leftFish.Split() {
			return true
		}

	} else if s.leftNumber >= 10 {
		s.leftFish = SplitFish(s.leftNumber)
		s.leftFish.parent = s
		s.leftNumber = 0
		return true

	}

	if s.rightFish != nil {
		if s.rightFish.Split() {
			return true
		}

	} else if s.rightNumber >= 10 {
		s.rightFish = SplitFish(s.rightNumber)
		s.rightFish.parent = s
		s.rightNumber = 0
		return true
	}

	return false
}

func SplitFish(n int) *SnailfishNumber {
	return &SnailfishNumber{leftNumber: n / 2, rightNumber: n - (n / 2)}
}

func (s *SnailfishNumber) AddLeft(n int) {
	if s.leftFish != nil {
		s.leftFish.AddLeft(n)
	} else {
		s.leftNumber += n
	}
}

func (s *SnailfishNumber) AddRight(n int) {
	if s.rightFish != nil {
		s.rightFish.AddRight(n)
	} else {
		s.rightNumber += n
	}
}

func (s *SnailfishNumber) Magnitude() int {
	var mag int
	if s.leftFish != nil {
		mag += 3 * s.leftFish.Magnitude()
	} else {
		mag += 3 * s.leftNumber
	}
	if s.rightFish != nil {
		mag += 2 * s.rightFish.Magnitude()
	} else {
		mag += 2 * s.rightNumber
	}
	return mag
}
