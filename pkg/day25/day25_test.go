package day25_test

import (
	"testing"

	"advent-of-code-2021/pkg/day25"
)

func TestSeaCucumberShuffle(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"v...>>.vv>",
			".vv>>.vv..",
			">>.>v>...v",
			">>v>>.>.v.",
			"v>v.vv.v..",
			">.>>..v...",
			".vv..>.>v.",
			"v.v..>>v.v",
			"....v..v.>",
		}, 58},
	}

	for _, test := range tests {
		actual := day25.SeaCucumberShuffle(test.in)
		if actual != test.out {
			t.Errorf("SeaCucumberShuffle(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
