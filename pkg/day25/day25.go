package day25

import (
	utils2020 "advent-of-code-2020/pkg/utils"
)

func SeaCucumberShuffle(input []string) int {
	herds := Parse(input)
	i := 1

	for herds.Move() {
		i++
	}

	return i
}

type Herds struct {
	Ocean map[utils2020.Coordinate2]rune
	Max   utils2020.Coordinate2
}

func Parse(input []string) Herds {
	h := Herds{
		Ocean: make(map[utils2020.Coordinate2]rune),
	}

	for y, line := range input {
		h.Max.Y = y

		for x, r := range line {
			h.Max.X = x

			switch r {
			case '>', 'v':
				h.Ocean[utils2020.Coordinate2{X: x, Y: y}] = r
			}
		}
	}

	return h
}

func (h *Herds) Step(cucumber rune, direction utils2020.Coordinate2) bool {
	var moved bool
	newOcean := make(map[utils2020.Coordinate2]rune)

	for pos, r := range h.Ocean {
		if r == cucumber {
			newPos := h.NewPos(pos, direction)

			if _, ok := h.Ocean[newPos]; !ok {
				moved = true
				newOcean[newPos] = r
			} else {
				newOcean[pos] = r
			}
		} else {
			newOcean[pos] = r
		}
	}

	h.Ocean = newOcean

	return moved
}

func (h *Herds) Move() bool {
	movedEast := h.Step('>', utils2020.East)
	movedSouth := h.Step('v', utils2020.South)
	return movedEast || movedSouth
}

func (h Herds) NewPos(pos, direction utils2020.Coordinate2) utils2020.Coordinate2 {
	newPos := pos.Move(direction)

	if newPos.X > h.Max.X {
		newPos.X = 0
	}

	if newPos.Y > h.Max.Y {
		newPos.Y = 0
	}

	return newPos
}
