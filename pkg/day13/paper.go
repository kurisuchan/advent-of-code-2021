package day13

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"regexp"
	"strings"
)

type Axis uint8

const (
	AxisX Axis = iota + 1
	AxisY
)

type Fold struct {
	Axis Axis
	Pos  int
}

type Paper map[utils2020.Coordinate2]struct{}

var (
	void = struct{}{}

	dotMatch  = regexp.MustCompile(`(\d+),(\d+)`)
	foldMatch = regexp.MustCompile(`fold along ([xy])=(\d+)`)
)

func Parse(input string) (Paper, []Fold) {
	paper := make(Paper)
	var folds []Fold

	for _, pair := range dotMatch.FindAllStringSubmatch(input, -1) {
		paper[utils2020.Coordinate2{
			X: utils2020.MustInt(pair[1]),
			Y: utils2020.MustInt(pair[2]),
		}] = struct{}{}
	}

	for _, ins := range foldMatch.FindAllStringSubmatch(input, -1) {
		fold := Fold{Pos: utils2020.MustInt(ins[2])}
		switch ins[1] {
		case "x":
			fold.Axis = AxisX
		case "y":
			fold.Axis = AxisY
		}
		folds = append(folds, fold)
	}

	return paper, folds
}

func (p Paper) Fold(fold Fold) {
	for pos := range p {
		var fptr *int

		switch fold.Axis {
		case AxisX:
			fptr = &pos.X
		case AxisY:
			fptr = &pos.Y
		}

		if fptr != nil && *fptr > fold.Pos {
			delete(p, pos)
			*fptr = fold.Pos - (*fptr - fold.Pos)
			p[pos] = void
		}
	}
}

func (p Paper) String() string {
	var maxX, maxY int
	for pos := range p {
		if pos.X > maxX {
			maxX = pos.X
		}
		if pos.Y > maxY {
			maxY = pos.Y
		}
	}

	var out strings.Builder
	out.Grow(maxX * maxY)

	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			if _, ok := p[utils2020.Coordinate2{X: x, Y: y}]; ok {
				out.WriteRune('#')
			} else {
				out.WriteRune('.')
			}
		}
		out.WriteRune('\n')
	}

	return out.String()
}
