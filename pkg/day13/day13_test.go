package day13_test

import (
	"testing"

	"advent-of-code-2021/pkg/day13"
)

func TestFirstFold(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5",
			17},
	}

	for _, test := range tests {
		actual := day13.FirstFold(test.in)
		if actual != test.out {
			t.Errorf("FirstFold(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAllTheFolds(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{
			"6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7",
			"#.##..#..#.\n#...#......\n......#...#\n#...#......\n.#.#..#.###\n",
		},
		{
			"6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5",
			"#####\n#...#\n#...#\n#...#\n#####\n",
		},
	}

	for _, test := range tests {
		actual := day13.AllTheFolds(test.in)
		if actual != test.out {
			t.Errorf("AllTheFolds(%q) => \n%s want \n%s", test.in, actual, test.out)
		}
	}
}
