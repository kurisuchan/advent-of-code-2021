package day13

func FirstFold(input string) int {
	paper, folds := Parse(input)

	paper.Fold(folds[0])

	return len(paper)
}

func AllTheFolds(input string) string {
	paper, folds := Parse(input)

	for _, fold := range folds {
		paper.Fold(fold)
	}

	return paper.String()
}
