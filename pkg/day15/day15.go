package day15

import (
	utils2019 "advent-of-code-2019/pkg/utils"
	utils2020 "advent-of-code-2020/pkg/utils"
	"math"
)

func ExitRisk(input []string) int {
	cavern := NewCavern(input)
	return cavern.Path()
}

func ExpandedExitRisk(input []string) int {
	cavern := NewCavern(input)
	cavern = cavern.Expand()
	return cavern.Path()
}

type Cavern struct {
	grid map[utils2020.Coordinate2]uint8
	dims utils2020.Coordinate2
}

func NewCavern(input []string) Cavern {
	grid := make(map[utils2020.Coordinate2]uint8)

	var x, y int
	for y = range input {
		for x = range input[y] {
			grid[utils2020.Coordinate2{X: x, Y: y}] = input[y][x] - '0'
		}
	}

	return Cavern{
		grid: grid,
		dims: utils2020.Coordinate2{X: x + 1, Y: y + 1},
	}
}

func (c Cavern) Expand() Cavern {
	for oy := 0; oy < c.dims.Y; oy++ {
		for ox := 0; ox < c.dims.X; ox++ {
			for i := 0; i < 5; i++ {
				for j := 0; j < 5; j++ {
					pos := utils2020.Coordinate2{X: i*c.dims.X + ox, Y: j*c.dims.Y + oy}
					c.grid[pos] = c.grid[utils2020.Coordinate2{X: ox, Y: oy}] + uint8(i) + uint8(j)
					if c.grid[pos] > 9 {
						c.grid[pos] -= 9
					}
				}
			}
		}
	}
	c.dims = utils2020.Coordinate2{
		X: 4*c.dims.X + c.dims.X,
		Y: 4*c.dims.Y + c.dims.Y,
	}

	return c
}

func (c Cavern) Path() int {
	queue := utils2019.NewQueue()
	totalRisk := make(map[utils2020.Coordinate2]int)
	prev := make(map[utils2020.Coordinate2]utils2020.Coordinate2)
	from := utils2020.Coordinate2{}
	to := utils2020.Coordinate2{X: c.dims.X - 1, Y: c.dims.Y - 1}

	for pos := range c.grid {
		totalRisk[pos] = math.MaxInt
	}
	queue.Set(from, 0)
	totalRisk[from] = 0

	for !queue.IsEmpty() {
		qr, risk := queue.Pop()
		cur := qr.(utils2020.Coordinate2)

		for _, no := range utils2020.Coordinate2Neighbors4 {
			neighbor := cur.Move(no)

			if nextRisk, ok := c.grid[neighbor]; ok {
				alt := risk + int(nextRisk)

				if alt < totalRisk[neighbor] {
					totalRisk[neighbor] = alt
					prev[neighbor] = cur
					queue.Set(neighbor, alt)
				}
			}
		}
	}

	return totalRisk[to]
}
