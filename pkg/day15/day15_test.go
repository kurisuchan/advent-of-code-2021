package day15_test

import (
	"testing"

	"advent-of-code-2021/pkg/day15"
)

func TestExitRisk(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"1163751742",
			"1381373672",
			"2136511328",
			"3694931569",
			"7463417111",
			"1319128137",
			"1359912421",
			"3125421639",
			"1293138521",
			"2311944581",
		}, 40},
	}

	for _, test := range tests {
		actual := day15.ExitRisk(test.in)
		if actual != test.out {
			t.Errorf("ExitRisk(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestExpandedExitRisk(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"1163751742",
			"1381373672",
			"2136511328",
			"3694931569",
			"7463417111",
			"1319128137",
			"1359912421",
			"3125421639",
			"1293138521",
			"2311944581",
		}, 315},
	}

	for _, test := range tests {
		actual := day15.ExpandedExitRisk(test.in)
		if actual != test.out {
			t.Errorf("ExpandedExitRisk(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
