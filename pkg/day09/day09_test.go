package day09_test

import (
	"testing"

	"advent-of-code-2021/pkg/day09"
)

func TestLowPoints(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"2199943210",
			"3987894921",
			"9856789892",
			"8767896789",
			"9899965678",
		}, 15},
	}

	for _, test := range tests {
		actual := day09.LowPoints(test.in)
		if actual != test.out {
			t.Errorf("LowPoints(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestBasins(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"2199943210",
			"3987894921",
			"9856789892",
			"8767896789",
			"9899965678",
		}, 1134},
	}

	for _, test := range tests {
		actual := day09.Basins(test.in)
		if actual != test.out {
			t.Errorf("Basins(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
