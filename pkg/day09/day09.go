package day09

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"sort"
)

func LowPoints(input []string) int {
	heightmap := ParseMap(input)

	var riskLevels int

	for pos, score := range heightmap {
		lowest := true

		for _, neighborPos := range utils2020.Coordinate2Neighbors4 {
			if neighborScore, ok := heightmap[pos.Move(neighborPos)]; ok {
				if neighborScore <= score {
					lowest = false
					break
				}
			}
		}

		if lowest {
			riskLevels += score + 1
		}
	}

	return riskLevels
}

func Basins(input []string) int {
	heightmap := ParseMap(input)

	var sizes []int

	for pos, score := range heightmap {
		if score == 9 {
			continue
		}

		sizes = append(sizes, heightmap.Basin(pos))
	}

	sort.Sort(sort.Reverse(sort.IntSlice(sizes)))

	return sizes[0] * sizes[1] * sizes[2]
}

type HeightMap map[utils2020.Coordinate2]int

func (hm HeightMap) Basin(pos utils2020.Coordinate2) int {
	var size int

	for _, c := range utils2020.Coordinate2Neighbors4 {
		neighborPos := pos.Move(c)

		if neighborScore, ok := hm[neighborPos]; ok {
			if neighborScore == 9 {
				continue
			}

			delete(hm, neighborPos)
			size += 1 + hm.Basin(neighborPos)
		}
	}

	return size
}

func ParseMap(input []string) HeightMap {
	out := make(HeightMap)

	for y := range input {
		for x, r := range input[y] {
			out[utils2020.Coordinate2{X: x, Y: y}] = utils2020.MustInt(string(r))
		}
	}

	return out
}
