package day20

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"math"
	"strings"
)

func Enhancement2(input string) float64 {
	image := Parse(input)

	image.Enhance()
	image.Enhance()

	return image.Count()
}

func Enhancement50(input string) float64 {
	image := Parse(input)

	for i := 0; i < 50; i++ {
		image.Enhance()
	}

	return image.Count()
}

type Image struct {
	Pixels    map[utils2020.Coordinate2]struct{}
	Algorithm string
	Min       utils2020.Coordinate2
	Max       utils2020.Coordinate2
	Outside   bool
}

func Parse(input string) Image {
	parts := strings.Split(strings.TrimSpace(input), "\n\n")
	enhancement := strings.TrimSpace(strings.ReplaceAll(parts[0], "\n", ""))

	image := Image{
		Pixels:    make(map[utils2020.Coordinate2]struct{}),
		Algorithm: enhancement,
	}

	for y, line := range strings.Split(strings.TrimSpace(parts[1]), "\n") {
		if y > image.Max.Y {
			image.Max.Y = y
		}

		for x, r := range strings.TrimSpace(line) {
			if x > image.Max.X {
				image.Max.X = x
			}

			if r == '#' {
				image.Pixels[utils2020.Coordinate2{X: x, Y: y}] = struct{}{}
			}
		}
	}

	return image
}

func (i *Image) Enhance() {
	pixels := make(map[utils2020.Coordinate2]struct{})
	var min, max utils2020.Coordinate2

	for y := i.Min.Y - 2; y <= i.Max.Y+2; y++ {
		for x := i.Min.X - 2; x <= i.Max.X+2; x++ {
			var lookup int

			for yy := -1; yy <= 1; yy++ {
				for xx := -1; xx <= 1; xx++ {
					lookup <<= 1

					if _, ok := i.Pixels[utils2020.Coordinate2{X: x + xx, Y: y + yy}]; ok ||
						(x+xx < i.Min.X || x+xx > i.Max.X || y+yy < i.Min.Y || y+yy > i.Max.Y) && i.Outside {
						lookup += 1
					}
				}
			}

			if i.Algorithm[lookup] == '#' {
				pixels[utils2020.Coordinate2{X: x, Y: y}] = struct{}{}

				if x < min.X {
					min.X = x
				}
				if x > max.X {
					max.X = x
				}
				if y < min.Y {
					min.Y = y
				}
				if y > max.Y {
					max.Y = y
				}
			}
		}
	}

	if i.Outside {
		i.Outside = i.Algorithm[0b111111111] == '#'
	} else {
		i.Outside = i.Algorithm[0] == '#'
	}

	i.Pixels, i.Min, i.Max = pixels, min, max
}

func (i Image) Count() float64 {
	if i.Outside {
		return math.Inf(1)
	}

	return float64(len(i.Pixels))
}
