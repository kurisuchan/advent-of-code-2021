package day19

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

func Beacons(beacons map[utils2020.Coordinate3]struct{}) int {
	return len(beacons)
}

func ScannerDistance(scannerPositions []utils2020.Coordinate3) int {
	var maxDist int

	for i := 0; i < len(scannerPositions); i++ {
		for j := 0; j < len(scannerPositions); j++ {
			if i == j {
				continue
			}

			dist := Distance(scannerPositions[i], scannerPositions[j])
			if dist > maxDist {
				maxDist = dist
			}
		}
	}

	return maxDist
}

func Parse(input string) []Scanner {
	var scanners []Scanner
	for _, derp := range strings.Split(strings.TrimSpace(input), "\n\n") {
		var scanner Scanner
		for _, line := range strings.Split(strings.TrimSpace(derp), "\n")[1:] {
			parts := strings.Split(line, ",")
			scanner = append(scanner, utils2020.Coordinate3{
				X: utils2020.MustInt(parts[0]),
				Y: utils2020.MustInt(parts[1]),
				Z: utils2020.MustInt(parts[2]),
			})
		}
		scanners = append(scanners, scanner)
	}

	return scanners
}

func Search(scanners []Scanner) ([]utils2020.Coordinate3, map[utils2020.Coordinate3]struct{}) {
	scannerPositions := []utils2020.Coordinate3{{}}
	beacons := make(map[utils2020.Coordinate3]struct{})

	for _, c := range scanners[0] {
		beacons[c] = struct{}{}
	}

	scanners = scanners[1:]

search:
	for len(scanners) > 0 {

		// for each remaining scanner
		for i := 0; i < len(scanners); i++ {
			// for each possible rotation
			for rotatedScanner := range scanners[i].Rotations() {
				offsets := make(map[utils2020.Coordinate3]int)
				var pos utils2020.Coordinate3

				for _, rotatedScannerBeacon := range rotatedScanner {
					for beacon := range beacons {
						pos = Sub(beacon, rotatedScannerBeacon)
						offsets[pos]++

						if offsets[pos] >= 12 {
							scannerPositions = append(scannerPositions, pos)
							scanners = append(scanners[:i], scanners[i+1:]...)

							for _, rotatedBeacon := range rotatedScanner {
								beacons[rotatedBeacon.Move(pos)] = struct{}{}
							}

							continue search
						}
					}
				}
			}
		}
	}

	return scannerPositions, beacons
}

type Scanner []utils2020.Coordinate3

func (s Scanner) Rotations() <-chan Scanner {
	o := make(chan Scanner, 0)

	go func(s Scanner) {
		o <- s
		for cycle := 0; cycle < 2; cycle++ {
			for rolls := 0; rolls < 3; rolls++ {
				s = s.Roll()
				o <- s

				for turns := 0; turns < 3; turns++ {
					s = s.Turn()
					o <- s
				}
			}

			s = s.Roll().Turn().Roll()
		}
		close(o)
	}(s)

	return o
}

func (s Scanner) Roll() Scanner {
	o := make(Scanner, len(s))

	for i := range s {
		o[i] = Roll(s[i])
	}

	return o
}

func (s Scanner) Turn() Scanner {
	o := make(Scanner, len(s))

	for i := range s {
		o[i] = Turn(s[i])
	}

	return o
}

func Roll(c utils2020.Coordinate3) utils2020.Coordinate3 {
	return utils2020.Coordinate3{
		X: c.X,
		Y: c.Z,
		Z: -c.Y,
	}
}

func Turn(c utils2020.Coordinate3) utils2020.Coordinate3 {
	return utils2020.Coordinate3{
		X: -c.Y,
		Y: c.X,
		Z: c.Z,
	}
}

func Sub(a, b utils2020.Coordinate3) utils2020.Coordinate3 {
	return utils2020.Coordinate3{
		X: a.X - b.X,
		Y: a.Y - b.Y,
		Z: a.Z - b.Z,
	}
}

func Distance(a, b utils2020.Coordinate3) int {
	return utils2020.AbsInt(a.X-b.X) + utils2020.AbsInt(a.Y-b.Y) + utils2020.AbsInt(a.Z-b.Z)
}
