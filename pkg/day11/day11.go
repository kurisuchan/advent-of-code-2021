package day11

import (
	utils2020 "advent-of-code-2020/pkg/utils"
)

func Flashes(input []string) int {
	g := NewGrid(input)
	var flashes int

	for i := 0; i < 100; i++ {
		flashes += g.Step()
	}

	return flashes
}

func Synchronized(input []string) int {
	g := NewGrid(input)

	var i int
	for {
		i++
		if g.Step() == 100 {
			return i
		}
	}
}

type Grid map[utils2020.Coordinate2]uint8

func NewGrid(input []string) Grid {
	g := make(Grid)

	for y := 0; y < 10; y++ {
		for x := 0; x < 10; x++ {
			g[utils2020.Coordinate2{X: x, Y: y}] = input[y][x] - '0'
		}
	}

	return g
}

func (g Grid) Step() int {
	for pos := range g {
		g[pos]++
	}

	flashed := make(map[utils2020.Coordinate2]struct{})

	newFlashes := true
	for newFlashes {
		newFlashes = false

		for pos, e := range g {
			if e > 9 {
				if _, found := flashed[pos]; !found {
					newFlashes = true
					flashed[pos] = struct{}{}

					for _, no := range utils2020.Coordinate2Neighbors8 {
						n := pos.Move(no)
						if _, ok := g[n]; ok {
							g[n]++
						}
					}
				}
			}
		}
	}

	for pos := range flashed {
		g[pos] = 0
	}

	return len(flashed)
}
