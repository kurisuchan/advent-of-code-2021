package day11_test

import (
	"testing"

	"advent-of-code-2021/pkg/day11"
)

func TestFlashes(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"5483143223",
			"2745854711",
			"5264556173",
			"6141336146",
			"6357385478",
			"4167524645",
			"2176841721",
			"6882881134",
			"4846848554",
			"5283751526",
		}, 1656},
	}

	for _, test := range tests {
		actual := day11.Flashes(test.in)
		if actual != test.out {
			t.Errorf("Flashes(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSynchronized(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"5483143223",
			"2745854711",
			"5264556173",
			"6141336146",
			"6357385478",
			"4167524645",
			"2176841721",
			"6882881134",
			"4846848554",
			"5283751526",
		}, 195},
	}

	for _, test := range tests {
		actual := day11.Synchronized(test.in)
		if actual != test.out {
			t.Errorf("Synchronized(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
