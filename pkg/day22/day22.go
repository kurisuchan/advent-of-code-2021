package day22

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/utils"
	"regexp"
)

func ReactorInitialization(input []string) int {
	steps := Parse(input)
	grid := make(map[utils2020.Coordinate3]struct{})

	for _, step := range steps {
		for z := utils.MaxInt(step.Min.Z, -50); z < utils.MinInt(step.Max.Z, 51); z++ {
			for y := utils.MaxInt(step.Min.Y, -50); y < utils.MinInt(step.Max.Y, 51); y++ {
				for x := utils.MaxInt(step.Min.X, -50); x < utils.MinInt(step.Max.X, 51); x++ {
					if step.On {
						grid[utils2020.Coordinate3{X: x, Y: y, Z: z}] = struct{}{}
					} else {
						delete(grid, utils2020.Coordinate3{X: x, Y: y, Z: z})
					}
				}
			}
		}
	}

	return len(grid)
}

func ReactorReboot(input []string) int {
	cubes := Parse(input)
	var distinctCubes []Cube

	for _, cube := range cubes {
		var newCubes []Cube

		for _, distinctCube := range distinctCubes {
			if cube.Overlaps(distinctCube) {

				// X
				if distinctCube.Min.X < cube.Min.X {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: distinctCube.Min.X,
							Y: distinctCube.Min.Y,
							Z: distinctCube.Min.Z,
						},
						Max: utils2020.Coordinate3{
							X: cube.Min.X,
							Y: distinctCube.Max.Y,
							Z: distinctCube.Max.Z,
						},
						On: distinctCube.On,
					})
					distinctCube.Min.X = cube.Min.X
				}

				if distinctCube.Max.X > cube.Max.X {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: cube.Max.X,
							Y: distinctCube.Min.Y,
							Z: distinctCube.Min.Z,
						},
						Max: utils2020.Coordinate3{
							X: distinctCube.Max.X,
							Y: distinctCube.Max.Y,
							Z: distinctCube.Max.Z,
						},
						On: distinctCube.On,
					})
					distinctCube.Max.X = cube.Max.X
				}

				// Y
				if distinctCube.Min.Y < cube.Min.Y {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: distinctCube.Min.X,
							Y: distinctCube.Min.Y,
							Z: distinctCube.Min.Z,
						},
						Max: utils2020.Coordinate3{
							X: distinctCube.Max.X,
							Y: cube.Min.Y,
							Z: distinctCube.Max.Z,
						},
						On: distinctCube.On,
					})
					distinctCube.Min.Y = cube.Min.Y
				}

				if distinctCube.Max.Y > cube.Max.Y {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: distinctCube.Min.X,
							Y: cube.Max.Y,
							Z: distinctCube.Min.Z,
						},
						Max: utils2020.Coordinate3{
							X: distinctCube.Max.X,
							Y: distinctCube.Max.Y,
							Z: distinctCube.Max.Z,
						},
						On: distinctCube.On,
					})
					distinctCube.Max.Y = cube.Max.Y
				}

				// Z
				if distinctCube.Min.Z < cube.Min.Z {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: distinctCube.Min.X,
							Y: distinctCube.Min.Y,
							Z: distinctCube.Min.Z,
						},
						Max: utils2020.Coordinate3{
							X: distinctCube.Max.X,
							Y: distinctCube.Max.Y,
							Z: cube.Min.Z,
						},
						On: distinctCube.On,
					})
				}

				if distinctCube.Max.Z > cube.Max.Z {
					newCubes = append(newCubes, Cube{
						Min: utils2020.Coordinate3{
							X: distinctCube.Min.X,
							Y: distinctCube.Min.Y,
							Z: cube.Max.Z,
						},
						Max: utils2020.Coordinate3{
							X: distinctCube.Max.X,
							Y: distinctCube.Max.Y,
							Z: distinctCube.Max.Z,
						},
						On: distinctCube.On,
					})
				}
			} else {
				newCubes = append(newCubes, distinctCube)
			}
		}

		distinctCubes = append(newCubes, cube)
	}

	var total int
	for _, cube := range distinctCubes {
		if cube.On {
			total += cube.Size()
		}
	}

	return total
}

type Cube struct {
	Min utils2020.Coordinate3
	Max utils2020.Coordinate3
	On  bool
}

func (c Cube) Overlaps(other Cube) bool {
	return utils.MaxInt(c.Min.X, other.Min.X) < utils.MinInt(c.Max.X, other.Max.X) &&
		utils.MaxInt(c.Min.Y, other.Min.Y) < utils.MinInt(c.Max.Y, other.Max.Y) &&
		utils.MaxInt(c.Min.Z, other.Min.Z) < utils.MinInt(c.Max.Z, other.Max.Z)
}

func (c Cube) Size() int {
	return (c.Max.X - c.Min.X) * (c.Max.Y - c.Min.Y) * (c.Max.Z - c.Min.Z)
}

var linePattern = regexp.MustCompile(`(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)`)

func Parse(input []string) []Cube {
	var cubes []Cube

	for _, line := range input {
		for _, parts := range linePattern.FindAllStringSubmatch(line, -1) {
			cubes = append(cubes, Cube{
				Min: utils2020.Coordinate3{
					X: utils2020.MustInt(parts[2]),
					Y: utils2020.MustInt(parts[4]),
					Z: utils2020.MustInt(parts[6]),
				},
				Max: utils2020.Coordinate3{
					X: utils2020.MustInt(parts[3]) + 1,
					Y: utils2020.MustInt(parts[5]) + 1,
					Z: utils2020.MustInt(parts[7]) + 1,
				},
				On: parts[1] == "on",
			})
		}
	}

	return cubes
}
