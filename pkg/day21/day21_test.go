package day21_test

import (
	"testing"

	"advent-of-code-2021/pkg/day21"
)

func TestDeterministicZocchihedron(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Player 1 starting position: 4\nPlayer 2 starting position: 8", 739785},
		{"Player 1 starting position: 1\nPlayer 2 starting position: 5", 432450},
	}

	for _, test := range tests {
		actual := day21.DeterministicZocchihedron(test.in)
		if actual != test.out {
			t.Errorf("DeterministicZocchihedron(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestQuantumTetrahedon(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Player 1 starting position: 4\nPlayer 2 starting position: 8", 444356092776315},
		{"Player 1 starting position: 1\nPlayer 2 starting position: 5", 138508043837521},
	}

	for _, test := range tests {
		actual := day21.QuantumTetrahedon(test.in)
		if actual != test.out {
			t.Errorf("QuantumTetrahedon(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRollDie(t *testing.T) {
	tests := []struct {
		in   int
		out  int
		next int
	}{
		{1, 6, 4},
		{4, 15, 7},
	}

	for _, test := range tests {
		actual, next := day21.RollDie(test.in)
		if actual != test.out || next != test.next {
			t.Errorf("RollDie(%d) => %d, %d, want %d, %d", test.in, actual, next, test.out, test.next)
		}
	}
}
