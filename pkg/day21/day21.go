package day21

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2021/pkg/utils"
	"regexp"
)

func DeterministicZocchihedron(input string) int {
	players := Parse(input)
	scores := make([]int, len(players))
	die := 1
	var rolls int
	var move int
	for {
		for p := range players {
			move, die = RollDie(die)
			rolls += 3
			players[p] = (players[p] + move) % 10
			scores[p] += players[p] + 1

			if scores[p] >= 1000 {
				return rolls * scores[(p+1)%len(players)]
			}
		}
	}
}

func QuantumTetrahedon(input string) int {
	players := Parse(input)
	w1, w2 := RecursiveGame(players[0], players[1], 0, 0, make(map[State][2]int))
	return utils.MaxInt(w1, w2)
}

type State struct {
	Pos1   int
	Pos2   int
	Score1 int
	Score2 int
}

func RecursiveGame(pos1, pos2, score1, score2 int, cache map[State][2]int) (int, int) {
	if s, ok := cache[State{pos1, pos2, score1, score2}]; ok {
		return s[0], s[1]
	}

	var wins1, wins2 int

	for die1 := 1; die1 <= 3; die1++ {
		for die2 := 1; die2 <= 3; die2++ {
			for die3 := 1; die3 <= 3; die3++ {
				newPos := (pos1 + die1 + die2 + die3) % 10
				newScore := score1 + newPos + 1

				if newScore >= 21 {
					wins1++
				} else {
					win1, win2 := RecursiveGame(pos2, newPos, score2, newScore, cache)
					wins1 += win2
					wins2 += win1
				}

			}
		}
	}

	cache[State{pos1, pos2, score1, score2}] = [2]int{wins1, wins2}

	return wins1, wins2
}

var playerStarts = regexp.MustCompile(`Player \d+ starting position: (\d+)`)

func Parse(input string) []int {
	var players []int
	m := playerStarts.FindAllStringSubmatch(input, 2)
	for i := range m {
		players = append(players, utils2020.MustInt(m[i][1])-1)
	}

	return players
}

func RollDie(n int) (int, int) {
	var sum int
	for i := 0; i < 3; i++ {
		sum += n
		n++
		if n > 100 {
			n = 1
		}
	}
	return sum, n
}
