package day24_test

import (
	"testing"

	"advent-of-code-2021/pkg/day24"
)

// Is this cheating?

func TestLargestModelNumber(t *testing.T) {
	tests := []struct {
		out int
	}{
		{98998519596997},
	}

	for _, test := range tests {
		actual := day24.LargestModelNumber()
		if actual != test.out {
			t.Errorf("LargestModelNumber() => %d, want %d", actual, test.out)
		}
	}
}

func TestSmallestModelNumber(t *testing.T) {
	tests := []struct {
		out int
	}{
		{31521119151421},
	}

	for _, test := range tests {
		actual := day24.SmallestModelNumber()
		if actual != test.out {
			t.Errorf("SmallestModelNumber() => %d, want %d", actual, test.out)
		}
	}
}
