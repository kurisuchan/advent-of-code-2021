package day24

func LargestModelNumber() int {
	var result int

outer:
	for N := 9; N > 0; N-- {
		for M := 9; M > 0; M-- {
			for L := 9; L > 0; L-- {
				for K := 9; K > 0; K-- {
					if L != K+3 {
						continue
					}
					for J := 9; J > 0; J-- {
						for I := 9; I > 0; I-- {
							if J != I+4 {
								continue
							}
							for H := 9; H > 0; H-- {
								for G := 9; G > 0; G-- {
									if H != G+8 {
										continue
									}
									for F := 9; F > 0; F-- {
										for E := 9; E > 0; E-- {
											for D := 9; D > 0; D-- {
												if D-1 != E {
													continue
												}
												for C := 9; C > 0; C-- {
													if F != C-4 {
														continue
													}
													for B := 9; B > 0; B-- {
														if M != B+1 {
															continue
														}
														for A := 9; A > 0; A-- {
															if N != A-2 {
																continue
															}
															result = A*10000000000000 +
																B*1000000000000 +
																C*100000000000 +
																D*10000000000 +
																E*1000000000 +
																F*100000000 +
																G*10000000 +
																H*1000000 +
																I*100000 +
																J*10000 +
																K*1000 +
																L*100 +
																M*10 +
																N
															break outer
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return result
}

func SmallestModelNumber() int {
	var result int

outer:
	for N := 1; N < 10; N++ {
		for M := 1; M < 10; M++ {
			for L := 1; L < 10; L++ {
				for K := 1; K < 10; K++ {
					if L != K+3 {
						continue
					}
					for J := 1; J < 10; J++ {
						for I := 1; I < 10; I++ {
							if J != I+4 {
								continue
							}
							for H := 1; H < 10; H++ {
								for G := 1; G < 10; G++ {
									if H != G+8 {
										continue
									}
									for F := 1; F < 10; F++ {
										for E := 1; E < 10; E++ {
											for D := 1; D < 10; D++ {
												if D-1 != E {
													continue
												}
												for C := 1; C < 10; C++ {
													if F != C-4 {
														continue
													}
													for B := 1; B < 10; B++ {
														if M != B+1 {
															continue
														}
														for A := 1; A < 10; A++ {
															if N != A-2 {
																continue
															}
															result = A*10000000000000 +
																B*1000000000000 +
																C*100000000000 +
																D*10000000000 +
																E*1000000000 +
																F*100000000 +
																G*10000000 +
																H*1000000 +
																I*100000 +
																J*10000 +
																K*1000 +
																L*100 +
																M*10 +
																N
															break outer
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	return result
}
