package utils_test

import (
	"advent-of-code-2021/pkg/utils"
	"testing"
)

func TestTriangularNumber(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{0, 0},
		{1, 1},
		{2, 3},
		{3, 6},
		{4, 10},
		{5, 15},
		{6, 21},
		{7, 28},
		{8, 36},
		{9, 45},
		{10, 55},
	}

	for _, test := range tests {
		actual := utils.TriangularNumber(test.in)
		if actual != test.out {
			t.Errorf("TriangularNumber(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMinInt(t *testing.T) {
	tests := []struct {
		x   int
		y   int
		out int
	}{
		{0, 0, 0},
		{1, 2, 1},
		{3, 2, 2},
	}

	for _, test := range tests {
		actual := utils.MinInt(test.x, test.y)
		if actual != test.out {
			t.Errorf("MinInt(%d, %d) => %d, want %d", test.x, test.y, actual, test.out)
		}
	}
}

func TestMaxInt(t *testing.T) {
	tests := []struct {
		x   int
		y   int
		out int
	}{
		{0, 0, 0},
		{1, 2, 2},
		{3, 2, 3},
	}

	for _, test := range tests {
		actual := utils.MaxInt(test.x, test.y)
		if actual != test.out {
			t.Errorf("MaxInt(%d, %d) => %d, want %d", test.x, test.y, actual, test.out)
		}
	}
}
