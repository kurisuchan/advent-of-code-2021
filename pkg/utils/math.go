package utils

func TriangularNumber(n int) int {
	return n * (n + 1) / 2
}

func MinInt(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func MaxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}
