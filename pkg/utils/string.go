package utils

import "sort"

type SortableRunes []rune

func (s SortableRunes) Less(i, j int) bool {
	return s[i] < s[j]
}

func (s SortableRunes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s SortableRunes) Len() int {
	return len(s)
}

func SortString(s string) string {
	r := []rune(s)
	sort.Sort(SortableRunes(r))
	return string(r)
}
