package utils_test

import (
	"advent-of-code-2021/pkg/utils"
	"testing"
)

func TestSortString(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"", ""},
		{"a", "a"},
		{"cba", "abc"},
		{"cab", "abc"},
		{"abc", "abc"},
		{"bca", "abc"},
		{"bac", "abc"},
		{"abacad", "aaabcd"},
	}

	for _, test := range tests {
		actual := utils.SortString(test.in)
		if actual != test.out {
			t.Errorf("SortString(%s) => %s, want %s", test.in, actual, test.out)
		}
	}
}
