package day14_test

import (
	"testing"

	"advent-of-code-2021/pkg/day14"
)

func TestPairInsertion(t *testing.T) {
	tests := []struct {
		in     []string
		amount int
		out    int
	}{
		{
			[]string{
				"NNCB",
				"",
				"CH -> B",
				"HH -> N",
				"CB -> H",
				"NH -> C",
				"HB -> C",
				"HC -> B",
				"HN -> C",
				"NN -> C",
				"BH -> H",
				"NC -> B",
				"NB -> B",
				"BN -> B",
				"BB -> N",
				"BC -> B",
				"CC -> N",
				"CN -> C",
				"",
			},
			10,
			1588,
		},
		{
			[]string{
				"NNCB",
				"",
				"CH -> B",
				"HH -> N",
				"CB -> H",
				"NH -> C",
				"HB -> C",
				"HC -> B",
				"HN -> C",
				"NN -> C",
				"BH -> H",
				"NC -> B",
				"NB -> B",
				"BN -> B",
				"BB -> N",
				"BC -> B",
				"CC -> N",
				"CN -> C",
			},
			0,
			1,
		},
		{
			[]string{
				"NNCB",
				"",
				"CH -> B",
				"HH -> N",
				"CB -> H",
				"NH -> C",
				"HB -> C",
				"HC -> B",
				"HN -> C",
				"NN -> C",
				"BH -> H",
				"NC -> B",
				"NB -> B",
				"BN -> B",
				"BB -> N",
				"BC -> B",
				"CC -> N",
				"CN -> C",
			},
			1,
			1,
		},
		{
			[]string{
				"NNCB",
				"",
				"CH -> B",
				"HH -> N",
				"CB -> H",
				"NH -> C",
				"HB -> C",
				"HC -> B",
				"HN -> C",
				"NN -> C",
				"BH -> H",
				"NC -> B",
				"NB -> B",
				"BN -> B",
				"BB -> N",
				"BC -> B",
				"CC -> N",
				"CN -> C",
			},
			2,
			5,
		},
		{
			[]string{
				"NNCB",
				"",
				"CH -> B",
				"HH -> N",
				"CB -> H",
				"NH -> C",
				"HB -> C",
				"HC -> B",
				"HN -> C",
				"NN -> C",
				"BH -> H",
				"NC -> B",
				"NB -> B",
				"BN -> B",
				"BB -> N",
				"BC -> B",
				"CC -> N",
				"CN -> C",
			},
			40,
			2188189693529,
		},
	}

	for _, test := range tests {
		actual := day14.PairInsertion(test.in, test.amount)
		if actual != test.out {
			t.Errorf("PairInsertion(%q, %d) => %d, want %d", test.in, test.amount, actual, test.out)
		}
	}
}
