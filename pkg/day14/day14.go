package day14

import (
	"strings"
)

type Rules map[string]rune

func PairInsertion(input []string, amount int) int {
	template, rules := Parse(input)

	counts := make(map[string]int)

	for i := 0; i < len(template)-1; i++ {
		counts[template[i:i+2]]++
	}

	for i := 0; i < amount; i++ {
		newCounts := make(map[string]int)

		for pair, count := range counts {
			newCounts[string([]rune{rune(pair[0]), rules[pair]})] += count
			newCounts[string([]rune{rules[pair], rune(pair[1])})] += count
		}

		counts = newCounts
	}

	score := make(map[rune]int)

	for k, v := range counts {
		score[rune(k[0])] += v
	}
	score[rune(template[len(template)-1])]++

	var least, most rune
	for r := range score {
		if most == 0 || score[r] > score[most] {
			most = r
		}
		if least == 0 || score[r] < score[least] {
			least = r
		}
	}

	return score[most] - score[least]
}

func Parse(input []string) (string, Rules) {
	template := strings.TrimSpace(input[0])
	rules := make(Rules)

	for _, line := range input[2:] {
		parts := strings.Split(line, " -> ")
		if len(parts) != 2 {
			continue
		}

		rules[strings.TrimSpace(parts[0])] = rune(parts[1][0])
	}

	return template, rules
}
