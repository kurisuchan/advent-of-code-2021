package day08

import (
	"advent-of-code-2021/pkg/utils"
	"strings"
)

func EasyDigits(input []string) int {
	notes := ParseNotes(input)
	var count int

	for _, note := range notes {
		for i := range note.Output {
			switch len(note.Output[i]) {
			case 2, 3, 4, 7:
				count++
			}
		}
	}

	return count
}

func DisplayOutputs(input []string) int {
	notes := ParseNotes(input)
	var out int

	for _, note := range notes {
		var outputs [10]string

		for i := range note.All {
			switch len(note.All[i]) {
			case 2:
				outputs[1] = note.All[i]
			case 3:
				outputs[7] = note.All[i]
			case 4:
				outputs[4] = note.All[i]
			case 7:
				outputs[8] = note.All[i]
			}
		}

		var oc, of rune

		for _, s := range note.All {
			if len(s) == 6 {
				// 0, 6 or 9
				for _, r := range outputs[1] {
					if !strings.ContainsRune(s, r) {
						oc = r
						outputs[6] = s
						break
					}
				}
			}
		}

		for _, r := range outputs[1] {
			if r != oc {
				of = r
			}
		}

		for _, s := range note.All {
			if len(s) == 5 {
				// 2, 3 or 5
				if !strings.ContainsRune(s, of) {
					outputs[2] = s
				} else if !strings.ContainsRune(s, oc) {
					outputs[5] = s
				} else {
					outputs[3] = s
				}

			}
		}

		for _, s := range note.All {
			if len(s) == 6 {
				if s == outputs[6] {
					continue
				}
				// 0 or 9
				if s == utils.SortString(string(append([]rune(outputs[5]), oc))) {
					outputs[9] = s
				} else {
					outputs[0] = s
				}
			}
		}

		digits := make(map[string]int)
		for i := 0; i < 10; i++ {
			digits[outputs[i]] = i
		}

		var o int
		for _, s := range note.Output {
			o *= 10
			o += digits[s]
		}

		out += o
	}

	return out
}

type Note struct {
	All    []string
	Output []string
}

func ParseNotes(input []string) []Note {
	var out []Note

	for _, line := range input {
		fields := strings.Fields(strings.TrimSpace(line))
		if len(fields) != 15 {
			continue
		}

		var n Note

		for i, f := range fields {
			n.All = append(n.All, utils.SortString(f))
			if i > 10 {
				n.Output = append(n.Output, utils.SortString(f))
			}
		}

		out = append(out, n)
	}

	return out
}
