package day01_test

import (
	"testing"

	"advent-of-code-2021/pkg/day01"
)

func TestDepthIncrease(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			199,
			200,
			208,
			210,
			200,
			207,
			240,
			269,
			260,
			263,
		}, 7},
	}

	for _, test := range tests {
		actual := day01.DepthIncrease(test.in)
		if actual != test.out {
			t.Errorf("DepthIncrease(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSlidingWindowIncrease(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			199,
			200,
			208,
			210,
			200,
			207,
			240,
			269,
			260,
			263,
		}, 5},
	}

	for _, test := range tests {
		actual := day01.SlidingWindowIncrease(test.in)
		if actual != test.out {
			t.Errorf("SlidingWindowIncrease(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
