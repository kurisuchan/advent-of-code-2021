package day01

func DepthIncrease(input []int) int {
	var increases int

	for i := 1; i < len(input); i++ {
		if input[i-1] < input[i] {
			increases++
		}
	}

	return increases
}

func SlidingWindowIncrease(input []int) int {
	var increases int

	for i := 3; i < len(input); i++ {
		if input[i-3] < input[i] {
			increases++
		}
	}

	return increases
}
