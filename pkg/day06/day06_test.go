package day06_test

import (
	"testing"

	"advent-of-code-2021/pkg/day06"
)

func TestLanternfish80(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"3,4,3,1,2", 5934},
	}

	for _, test := range tests {
		actual := day06.Lanternfish80(test.in)
		if actual != test.out {
			t.Errorf("Lanternfish80(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestLanternfish256(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"3,4,3,1,2", 26984457539},
	}

	for _, test := range tests {
		actual := day06.Lanternfish256(test.in)
		if actual != test.out {
			t.Errorf("Lanternfish256(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestFishyBusiness(t *testing.T) {
	tests := []struct {
		in   [9]int
		days int
		out  int
	}{
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 1, 5},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 2, 6},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 3, 7},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 4, 9},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 5, 10},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 6, 10},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 7, 10},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 8, 10},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 9, 11},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 10, 12},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 11, 15},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 12, 17},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 13, 19},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 14, 20},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 15, 20},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 16, 21},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 17, 22},
		{[9]int{0, 1, 1, 2, 1, 0, 0, 0, 0}, 18, 26},
	}

	for _, test := range tests {
		out := day06.FishyBusiness(test.in, test.days)
		actual := day06.SumFish(out)
		if actual != test.out {
			t.Errorf("FishyBusiness2(%v, %d) => %d, want %d", test.in, test.days, actual, test.out)
		}
	}
}
