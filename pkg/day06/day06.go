package day06

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

func Lanternfish80(input string) int {
	fish := GetFish(input)
	fish = FishyBusiness(fish, 80)
	return SumFish(fish)
}

func Lanternfish256(input string) int {
	fish := GetFish(input)
	fish = FishyBusiness(fish, 256)
	return SumFish(fish)
}

func GetFish(input string) [9]int {
	var fish [9]int

	for _, num := range strings.Split(input, ",") {
		fish[utils2020.MustInt(strings.TrimSpace(num))]++
	}

	return fish
}

func FishyBusiness(fish [9]int, days int) [9]int {
	for i := 0; i < days; i++ {
		newFish := fish[0]

		for p := 1; p < 9; p++ {
			fish[p-1] = fish[p]
		}

		fish[6] += newFish
		fish[8] = newFish
	}

	return fish
}

func SumFish(fish [9]int) int {
	var sum int
	for i := range fish {
		sum += fish[i]
	}
	return sum
}
