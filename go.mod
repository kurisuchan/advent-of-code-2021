module advent-of-code-2021

go 1.17

replace advent-of-code-2020 => gitlab.com/kurisuchan/advent-of-code-2020 v0.0.0-20201225055828-b67a782fc982

replace advent-of-code-2019 => gitlab.com/kurisuchan/advent-of-code-2019 v0.0.0-20191225062959-b4c1d15e9873

require advent-of-code-2020 v0.0.0-20201225055828-b67a782fc982

require advent-of-code-2019 v0.0.0-20191225062959-b4c1d15e9873
